<?php
$functions = TEMPLATEPATH . '/functions/';
// require_once TEMPLATEPATH . '/vendor/autoload.php';

include( $functions . 'misc.php' );
include( $functions . 'users.php' );
include( $functions . 'ss.php' );
include( $functions . 'admin.php' );
include( $functions . 'images.php' );
include( $functions . 'nav.php' );
include( $functions . 'shortcodes.php' );
include( $functions . 'cpts.php' );
include( $functions . 'ajax.php' );
include( $functions . 'stripe.php' );
include( $functions . 'mailchimp.php' );
include( $functions . 'csv.php' );
include( TEMPLATEPATH . '/templates/emails/emails.php' );

// $cacheExpiry = 12 * HOUR_IN_SECONDS;
// $cache       = new ES\Cache\TransientCache( $cacheExpiry );
// $retriever   = new ES\Retriever\Manager( $cache );
// $escaper     = new ES\Escaper\Manager();
// $meta        = new ES\Meta( $cache, $retriever, $escaper );

