<section class="page-section white form-page" id="form-page-2">
	<form class="subscription-form" action="subscription-form-page-2" method="post">
		<?php wp_nonce_field( 'es_ajax_form', 'es_ajax_form' ); ?>
		<div class="row large-collapse subscription-section">
			<div class="small-12 large-4 columns">
				<div class="subscription-method">
					<div class="row">
						<div class="small-12 medium-4 large-12 columns">
							<div class="subscription-box">
								<svg class="flourish top-left"><use xlink:href="#sub-box-top-left" /></svg>
								<svg class="flourish top-right"><use xlink:href="#sub-box-top-right" /></svg>
								<svg class="subscription-method-icon"><use xlink:href="#bottles-monthly" /></svg>
								<svg class="flourish bottom-left"><use xlink:href="#sub-box-bottom-left" /></svg>
								<svg class="flourish bottom-right"><use xlink:href="#sub-box-bottom-right" /></svg>
							</div>
						</div>
						<div class="small-12 medium-8 large-12 columns">
							<div class="subscription-details">
								<h2>Monthly</h2>
								<p class="secondary-color"><strong><em>Receive a different gin delivery every month</em></strong></p>
								<button type="submit" name="subscription-type" id="subscription-type-monthly" data-value="monthly" class="button block" href=""><strong>£35</strong> for 1 bottle <strong>every Month</strong></button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="small-12 large-4 columns">
				<div class="subscription-method">
					<div class="row">
						<div class="small-12 medium-4 large-12 columns">
							<div class="subscription-box">
								<svg class="flourish top-left"><use xlink:href="#sub-box-top-left" /></svg>
								<svg class="flourish top-right"><use xlink:href="#sub-box-top-right" /></svg>
								<svg class="subscription-method-icon"><use xlink:href="#bottles-bi-monthly" /></svg>
								<svg class="flourish bottom-left"><use xlink:href="#sub-box-bottom-left" /></svg>
								<svg class="flourish bottom-right"><use xlink:href="#sub-box-bottom-right" /></svg>
							</div>
						</div>
						<div class="small-12 medium-8 large-12 columns">
							<div class="subscription-details">
								<h2>Bi-Monthly</h2>
								<p class="secondary-color"><strong><em>Receive a different gin delivery every 2 months</em></strong></p>
								<button type="submit" name="subscription-type" id="subscription-type-bi-monthly" data-value="bi-monthly" class="button block" href=""><strong>£35</strong> for 1 bottle <strong>every 2 Months</strong></button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="small-12 large-4 columns">
				<div class="subscription-method">
					<div class="row">
						<div class="small-12 medium-4 large-12 columns">
							<div class="subscription-box">
								<svg class="flourish top-left"><use xlink:href="#sub-box-top-left" /></svg>
								<svg class="flourish top-right"><use xlink:href="#sub-box-top-right" /></svg>
								<svg class="subscription-method-icon"><use xlink:href="#bottles-quarterly" /></svg>
								<svg class="flourish bottom-left"><use xlink:href="#sub-box-bottom-left" /></svg>
								<svg class="flourish bottom-right"><use xlink:href="#sub-box-bottom-right" /></svg>
							</div>
						</div>
						<div class="small-12 medium-8 large-12 columns">
							<div class="subscription-details">
								<h2>Quarterly</h2>
								<p class="secondary-color"><strong><em>Receive a different gin delivery every 3 months</em></strong></p>
								<button type="submit" name="subscription-type" id="subscription-type-quarterly" data-value="quarterly" class="button block" href=""><strong>£35</strong> for 1 bottle <strong>every 3 Months</strong></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="form-page-number" id="form-page-number" value="2">
	</form>
</section>
