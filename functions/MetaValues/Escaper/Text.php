<?php

namespace ES\Escaper;

class Text extends EscaperAbstract
{
    public function handle($value)
    {
        return $this->kses($value);
    }
}
