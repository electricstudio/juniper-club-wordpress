<?php get_header();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>
		<section class="featured-img" style="background: url('<?php the_post_thumbnail_url('featured-img') ?>') center no-repeat; background-size: cover;"></section>
		<section class="page-section white">
			<div class="row">
				<div class="small-12 medium-9 medium-centered large-6">
					<p class="post-date"><?php the_time('jS, F Y') ?></p>
					<?php the_title( '<h1>', '</h1>' );
					the_content(); ?>
				</div>
			</div>
		</section>
	<?php }
}
get_footer();

