<?php

require_once TEMPLATEPATH . '/vendor/stripe/stripe-php/init.php';

// \Stripe\Stripe::setApiKey( 'sk_test_nbLFpcCO05WOqYrHeZ48s5Kx' );
\Stripe\Stripe::setApiKey( 'sk_live_soE50lapAKftduhqW6pC0c71' );

function stripe_customer( $req ) {

	if ( $req['subscription_method'] == 'gift' ) {

		$user_obj = get_userdata( $req['user_id'] );

		$token  = $req['str_token'];

		$meta_array = array(
			'first_name' => $req['first_name'],
			'last_name' => $req['last_name'],
			'contact_phone' => $req['contact_phone'],
			'delivery_address_1' => $req['delivery_address_1'],
			'delivery_address_2' => $req['delivery_address_2'],
			'delivery_city' => $req['delivery_city'],
			'delivery_county' => $req['delivery_county'],
			'delivery_postcode' => $req['delivery_postcode'],
			'subscription_date' => date( 'd-m-Y', get_next_cutoff() ),
		);

		$customer = \Stripe\Customer::create(array(
			'email' => $user_obj->user_email,
			'source'  => $token,
			'metadata' => $meta_array,
		));
	} else {
		switch ( $req['subscription_type'] ) {
			case 'monthly':
				$subscription_type = 'Sub12';
				break;
			case 'bi-monthly':
				$subscription_type = 'Sub6';
				break;
			case 'quarterly':
				$subscription_type = 'Sub4';
				break;
		}

		$user_obj = get_userdata( $req['user_id'] );

		$token  = $req['str_token'];

		$meta_array = array(
			'first_name' => $req['first_name'],
			'last_name' => $req['last_name'],
			'contact_phone' => $req['contact_phone'],
			'delivery_address_1' => $req['delivery_address_1'],
			'delivery_address_2' => $req['delivery_address_2'],
			'delivery_city' => $req['delivery_city'],
			'delivery_county' => $req['delivery_county'],
			'delivery_postcode' => $req['delivery_postcode'],
			'subscription_date' => date( 'd-m-Y', get_next_cutoff() ),
		);

		$stripe_args = array(
			'email' => $user_obj->user_email,
			'source'  => $token,
			'plan' => $subscription_type,
			'metadata' => $meta_array,
			'tax_percent' => 20,
			'prorate' => false,
		);

		if( get_next_cutoff() != strtotime( date('d-m-Y') ) ) {
			$stripe_args['trial_end'] = get_next_cutoff();
		}

		if ( $req['discount_code'] ) {
			$stripe_args['coupon'] = $req['discount_code'];
		}

		try {
			$customer = \Stripe\Customer::create( $stripe_args );
		}
		catch ( Exception $e )
		{
			$customer = array(
				'errors' => $e->getMessage()
			);
		}
	}
	return $customer;
}

function stripe_charge( $customer, $req ) {

	switch ( $req['subscription_type'] ) {
		case 'monthly':
			$price = 40000;
			$tax_amount = '£66.67';
			break;
		case 'bi-monthly':
			$price = 20500;
			$tax_amount = '£34.17';
			break;
		case 'quarterly':
			$price = 13500;
			$tax_amount = '£22.50';
			break;
	}

	$meta_array = array(
		'tax_amount' => $tax_amount,
		'recipient_first_name' => $req['recipient_first_name'],
		'recipient_last_name' => $req['recipient_last_name'],
		'recipient_contact_phone' => $req['recipient_contact_phone'],
		'recipient_address_1' => $req['recipient_address_1'],
		'recipient_address_2' => $req['recipient_address_2'],
		'recipient_city' => $req['recipient_city'],
		'recipient_county' => $req['recipient_county'],
		'recipient_postcode' => $req['recipient_postcode'],
		'recipient_email' => $req['recipient_email'],
		'subscription_type' => $req['subscription_type'],
		'subscription_date' => date( 'd-m-Y', get_next_cutoff() ),
	);

	$stripe_args = array(
		'customer' => $customer->id,
		'amount'   => $price,
		'currency' => 'gbp',
		'metadata' => $meta_array,
	);

	if ( $req['discount_code'] ) {
		$coupon = get_stripe_coupon( $req['discount_code'] );

		if( array_key_exists( 'errors', $coupon ) ){
			return $coupon;
		}

		$meta_array['coupon'] = $req['discount_code'];
		$meta_array['discount'] = $coupon->amount_off;
		$stripe_args['metadata'] = $meta_array;
		$stripe_args['amount'] = $price - $meta_array['discount'];
	}

	try {
		$charge = \Stripe\Charge::create( $stripe_args );
	}
	catch ( Exception $e )
	{
		$body = $e->getJsonBody();

		$charge = array(
			'errors' => $body['error'],
		);
	}

	return $charge;
}

function get_stripe_customer ( $customer_id ) {

	$customer_obj = \Stripe\Customer::retrieve( $customer_id );

	return $customer_obj;
}

function get_stripe_coupon ( $coupon_id ) {

	try {
		$coupon_obj = \Stripe\Coupon::retrieve( strtoupper( $coupon_id ) );
	}
	catch ( Exception $e)
	{
		$body = $e->getJsonBody();

		$coupon_obj = array(
			'errors' => $body['error'],
		);
	}
		return $coupon_obj;
}
