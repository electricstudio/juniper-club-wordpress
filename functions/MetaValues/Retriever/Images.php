<?php

namespace ES\Retriever;

class Images
{
    public function handle(array $value, $data)
    {
        $image_size   = 'full';

        if (array_key_exists(2, $data)) {
            $image_size = $data[2];
        }

        if (is_array($value)) {
            $image_array = [];

            foreach ($value as $image) {
                $image_array[] = wp_get_attachment_image_src($image, $image_size);
            }
        }

        return $image_array;
    }
}
