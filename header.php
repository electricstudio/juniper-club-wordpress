<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9" lang="en" > <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" > <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class() ?>>
		<?php include_once( 'build/images/svg/svg.svg' ); ?>
		<noscript>
			<div class="no-js-banner">
				<div class="row">
					<div class="small-12 columns">
						<p>
							<strong>You currently do not have JavaScript enabled. You may encounter issues with subscribing to the Juniper Club.</strong>
						</p>
					</div>
				</div>
			</div>
		</noscript>
		<header>
			<div class="row">
				<div class="small-12 columns">
					<?php if ( has_nav_menu( 'primary' ) ) {
						wp_nav_menu( array( 'theme_location'  => 'primary', ) );
					} ?>
				</div>
			</div>
			<a class="menu-button hide-for-large">Menu ☰</a>
		</header>