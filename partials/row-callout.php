<div class="row content-row">
	<div class="small-12 large-7 columns">
		<?php echo apply_filters('the_content', get_sub_field( 'call_out_row_content' ) ); ?>
	</div>
	<div class="small-12 large-5 columns">
		<div class="panel">
			<?php echo apply_filters('the_content', get_sub_field( 'callout_content' ) ); ?>
		</div>
	</div>
</div>