<?php

namespace ES\Retriever;

class Align
{
    public function handle($value)
    {
        if (empty($value)) {
            return 'text-left';
        }

        return "text-{$value}";
    }
}
