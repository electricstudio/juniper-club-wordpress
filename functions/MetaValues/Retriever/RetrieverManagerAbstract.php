<?php

namespace ES\Retriever;

use ES\Cache\CacheInterface;

abstract class RetrieverManagerAbstract
{
    protected $cache;

    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    public function getPostId()
    {
        global $post;

        if (function_exists('post_id_resolver')) {
            if ($id = call_user_func('post_id_resolver', $post_id)) {
                return $id;
            }
        }

        if (is_front_page()) {
            return get_option('page_on_front');
        }

        if (is_home()) {
            return get_option('page_for_posts');
        }

        if (is_archive()) {
            return get_option('page_for_posts');
        }

        if (is_object($post)) {
            return $post->ID;
        }

        return null;
    }

    protected function getPostMeta($retreiver, $meta_key)
    {
        $single = true;

        if (in_array($retreiver, $this->multiples)) {
            $single = false;
        }

        return get_post_meta($this->post_id, $meta_key, $single);
    }
}
