<?php

namespace ES;

use ES\Cache\CacheInterface;
use ES\Retriever\RetrieverManagerInterface;
use ES\Escaper\EscaperManagerInterface;

class Meta
{
    public function __construct(CacheInterface $cache, RetrieverManagerInterface $retriever, EscaperManagerInterface $escaper)
    {
        $this->cache     = $cache;
        $this->retriever = $retriever;
        $this->escaper   = $escaper;
    }

    public function get(array $meta_keys, $pid = null, $prefix = '_es_')
    {
        return $this->retriever->manage($meta_keys, $pid, $prefix);
    }

    public function cache()
    {
        return $this->cache;
    }

    public function esc($method, $value)
    {
        echo $this->escaper->manage($method, $value);
    }

    public static function display($value)
    {
        echo balanceTags($value);
    }
}
