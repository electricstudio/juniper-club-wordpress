<?php

namespace ES\Retriever;

class Content
{
    public function handle($value)
    {
        return apply_filters('the_content', $value);
    }
}
