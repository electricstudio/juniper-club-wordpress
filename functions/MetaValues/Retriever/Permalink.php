<?php

namespace ES\Retriever;

class Permalink
{
    public function handle($value)
    {
        return $value ? get_permalink(intval($value)) : null;
    }
}
