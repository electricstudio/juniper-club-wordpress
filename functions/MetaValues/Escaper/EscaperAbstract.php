<?php

namespace ES\Escaper;

abstract class EscaperAbstract
{
    public function kses($value)
    {
        return wp_kses_post($value);
    }
}
