<?php

namespace ES\Retriever;

class PostField
{
    public function handle($value, $data)
    {
        $field = false;

        if (array_key_exists(2, $data)) {
            $field = $data[2];
        }

        if (! $field) {
            return get_post(intval($value));
        }

        return $value ? get_post_field($field, intval($value)) : null;
    }
}
