<?php
function es_theme_enque_scripts() {
	$build = esc_html( TEMPLATEURI ) . 'build/js/';

	wp_register_script( 'main', $build . 'script.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'main' );

	wp_localize_script( 'main', 'ES_CONFIG', array(
		'site_url' => get_template_directory_uri(),
		'admin_url' => admin_url( 'admin-ajax.php' )
	) );

	wp_enqueue_script( 'jquery' );

	wp_register_script( 'stripe', 'https://js.stripe.com/v2/');
	wp_enqueue_script( 'stripe' );

	$stylesheet_uri = get_stylesheet_uri();

	// to clear the cache you must use a number greater than 1.
	$stylesheet_uri = add_query_arg( array( 'v' => 1 ), $stylesheet_uri );

	wp_register_style( 'main', $stylesheet_uri );
	wp_register_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Lora:400,700,700italic|Playfair+Display' );

	wp_enqueue_style( 'main' );
	wp_enqueue_style( 'google-fonts' );
}
add_action( 'wp_enqueue_scripts', 'es_theme_enque_scripts' );