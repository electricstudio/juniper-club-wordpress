<?php

namespace ES\Retriever;

class Post
{
    public function handle($value)
    {
        return get_post(intval($value));
    }
}
