<?php

namespace ES\Escaper;

class Manager implements EscaperManagerInterface
{
    public function manage($method, $value)
    {
        $class = str_replace(' ', '', ucwords(str_replace('_', ' ', $method)));
        $escaper = __NAMESPACE__ .'\\'. $class;
        return (new $escaper)->handle($value);
    }
}
