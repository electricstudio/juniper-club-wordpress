<?php

if ( array_key_exists('csv_export', $_GET) ) {
	es_csv();
}

function es_csv () {
	$today['stamp'] = time();
	$today['month'] = date( 'm', $today['stamp'] );
	$today['year'] = date( 'Y', $today['stamp'] );

	$i = 1;
	$subscriptions_array[0] = array(
		'customer_id',
		'name',
		'address_1',
		'address_2',
		'address_city',
		'address_county',
		'address_postcode',
		'contact_phone',
		'contact_email',
	);

	$args = array(
		'post_type' => 'subscriptions',
		'posts_per_page' => -1,
	);

	$subscriptions = new WP_Query( $args );

	if( $subscriptions->have_posts() ) {
		while ( $subscriptions->have_posts() ) {
			$subscriptions->the_post();

			$sub_date_field = get_field( 'sub_subscription_date' );
			$sub_date['stamp'] = strtotime( $sub_date_field );
			$sub_date['month'] = date( 'm', $sub_date['stamp'] );
			$sub_date['year'] = date( 'Y', $sub_date['stamp'] );

			$difference = ( ( $today['year'] - $sub_date['year'] ) * 12 ) + ( $today['month'] - $sub_date['month'] );

			$sub_type = get_field( 'sub_subscription_type' );

			$sub_method = get_field( 'sub_subscription_method' );

			switch ( $sub_type ) {
				case 'monthly':
						$modulus = 1;
					break;

				case 'bi-monthly':
						$modulus = 2;
					break;

				case 'quarterly':
						$modulus = 3;
					break;
			}

			if ( $difference >= 0 ) {
				$subscription_needed = $difference % $modulus;
			} else {
				$subscription_needed = -1;
			}

			if( $sub_method == 'gift' ) {
				if ( $sub_date['stamp'] < strtotime('1 year ago')) {
					$subscription_needed = -1;
				}
			}

			if( $subscription_needed == 0 ) {

				switch ( get_field( 'sub_subscription_method' ) ) {
					case 'gift':
						$subscriptions_array[$i] = array(
							'customer_id' => get_field( 'sub_customer_id' ),
							'name' => get_field( 'sub_recipient_first_name' ) . ' ' . get_field( 'sub_recipient_last_name' ),
							'address_1' => get_field( 'sub_recipient_address_1' ),
							'address_2' => get_field( 'sub_recipient_address_2' ),
							'address_city' => get_field( 'sub_recipient_city' ),
							'address_county' => get_field( 'sub_recipient_county' ),
							'address_postcode' => get_field( 'sub_recipient_postcode' ),
							'contact_phone' => '',
							'contact_email' => get_field( 'sub_recipient_email' )
						);
						break;

					default:
						$subscriptions_array[$i] = array(
							'customer_id' => get_field( 'sub_customer_id' ),
							'name' => get_field( 'sub_first_name' ) . ' ' . get_field( 'sub_last_name' ),
							'address_1' => get_field( 'sub_delivery_address_1' ),
							'address_2' => get_field( 'sub_delivery_address_2' ),
							'address_city' => get_field( 'sub_delivery_city' ),
							'address_county' => get_field( 'sub_delivery_county' ),
							'address_postcode' => get_field( 'sub_delivery_postcode' ),
							'contact_phone' => get_field( 'sub_contact_phone' ),
							'contact_email' => get_field( 'sub_recipient_email' )
						);

						if( !$subscriptions_array[$i]['contact_email'] ) {
							$subscriptions_array[$i]['contact_email'] = get_the_author_meta( 'email' );
						}
						break;
				}


				$i++;
			}
		}
	}
	wp_reset_postdata();


	$filename = 'export_' . current_time('YmdHis') . '.csv';
	$dir = maybe_create_directory();

	$handle = fopen( $dir . $filename, 'w+' );
	foreach ($subscriptions_array as $line) {
		fputcsv($handle, $line);
	}
	fclose($handle);

	$file_url = wp_upload_dir()['baseurl'] . '/juniper/subscriptions/' . $filename;
	// $file_url = '';

	return $file_url;
}

function maybe_create_directory() {

	$dirs = wp_upload_dir();

	$dir = trailingslashit( $dirs['basedir'] ) . 'juniper/subscriptions/';

	if( !is_dir( $dir ) || !is_writable( $dir ) ) {
		if( wp_mkdir_p( $dir ) ) {
			return $dir;
		}
		return false;
	}
	return $dir;
}

function export_subscription_csv() {
	$download_link = es_csv(); ?>
	<h1>Export Subscriptions CSV</h1>
	<p>To get this current month's subscriptions, press the download button below.</p>
	<a class="button" href="<?php echo $download_link ?>">Download Subscriptions</a>
<?php }

add_action('admin_menu', 'export_csv_page');

function export_csv_page() {
	add_menu_page( 'Export Subscription CSV', 'Export Subscriptions', 'read', 'es-csv-exports', 'export_subscription_csv' );
}
