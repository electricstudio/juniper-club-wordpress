<div class="row testimonial">
	<div class="small-12 columns">
		<img class="testimonial-img" src="<?php echo get_sub_field( 'testimonial_image' )['sizes']['testimonial-img']; ?>" >
		<blockquote>
			<p><?php echo get_sub_field( 'testimonial_quote' ); ?></p>
			<footer><?php echo get_sub_field( 'testimonial_source' ); ?></footer>
		</blockquote>
	</div>
</div>
