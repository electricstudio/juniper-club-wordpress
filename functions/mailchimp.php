<?php

/* Mail Chimp List IDs:
 *
 * Newsletter: 9813b3423e
 * Members: 84b022115b
 * Gift Purchasers: d13ec55c85
 * Gift Receivers: 8fe8a3a1f5
 *
 */

function subscribe_user_mailchimp( $data, $list_id = '84b022115b' ) {
	$api_key = 'e512e206e1849302fbe90b78551ba600-us13';

	$member_id = md5( strtolower( $data['email'] ) );
	$data_center = substr( $api_key,strpos( $api_key,'-' ) + 1 );
	$url = 'https://' . $data_center . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . $member_id;

	$json = json_encode( [
		'email_address' => $data['email'],
		'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
	] );

	$ch = curl_init( $url );

	curl_setopt( $ch, CURLOPT_USERPWD, 'user:' . $api_key );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json'] );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $json );

	$result = curl_exec( $ch );
	$http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
	curl_close( $ch );

	return $http_code;
}

function unsubscribe_user_mailchimp( $data, $list_id = '9813b3423e' ) {
	$api_key = 'e512e206e1849302fbe90b78551ba600-us13';

	$member_id = md5( strtolower( $data['email'] ) );
	$data_center = substr( $api_key,strpos( $api_key,'-' ) + 1 );
	$url = 'https://' . $data_center . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . $member_id;

	$json = json_encode( [
		'email_address' => $data['email'],
		'status'        => 'subscribed', // "subscribed","unsubscribed","cleaned","pending"
	] );

	$ch = curl_init( $url );

	curl_setopt( $ch, CURLOPT_USERPWD, 'user:' . $api_key );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json'] );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $json );

	$result = curl_exec( $ch );
	$http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
	curl_close( $ch );

	return $http_code;
}
