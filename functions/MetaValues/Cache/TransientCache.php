<?php

namespace ES\Cache;

class TransientCache extends CacheAbstract implements CacheInterface
{
    private $expiration;

    public function __construct($expiration) {
        $this->expiration = $expiration;
    }

    public function get($key, callable $resolver)
    {
        if (false === ($response = get_transient($key))) {
            $response = call_user_func($resolver);
            set_transient($key, $response, $this->expiration);
        }
        return $response;
    }

    public function put($key, $data)
    {
        set_transient($key, $data, $this->expiration);
    }

    public function forget($key)
    {
        delete_transient($key);
    }

    public function key($key)
    {
        return md5(serialize($key));
    }
}
