<?php
function es_empty_paragraph_fix( $content ) {
	// An array of the offending tags.
	$arr = array(
		'<p>[' => '[',
		']</p>' => ']',
		']<br />' => ']',
	);
	// Remove the offending tags and return the stripped content.
	$stripped_content = strtr( $content, $arr );
	return $stripped_content;
}
add_filter( 'the_content', 'es_empty_paragraph_fix' );
