<?php $page_sections = get_field( 'page_sections' );

if ( have_rows( 'page_sections' ) ) {
	while ( have_rows( 'page_sections' ) ) {
		the_row();
		$section_bg = get_sub_field( 'section_background' );
		if ( $section_bg == 'image' ) {
			$bg_image = get_sub_field( 'section_background_image' ); ?>
			<section class="page-section <?php echo $section_bg ?>" style="background: url('<?php echo $bg_image['sizes']['featured-img'] ?>') center no-repeat; background-size: cover;">
					<div class="overlay">
		<?php } else { ?>
			<section class="page-section <?php echo $section_bg ?>">
		<?php }

		if ( have_rows( 'section_content' ) ) {

			while ( have_rows( 'section_content' ) ) {
				the_row();

				switch ( get_row_layout() ) {
					case 'bordered_box':
						get_template_part( 'partials/bordered-box' );
						break;

					case 'latest_gin':
						get_template_part( 'partials/latest-gin' );
						break;

					case 'subscription_box':
						get_template_part( 'partials/subscriptions-box' );
						break;

					case 'gifts_box':
						get_template_part( 'partials/gifts-box' );
						break;

					case 'testimonial':
						get_template_part( 'partials/testimonial' );
						break;

					case 'latest_blog_post':
						get_template_part( 'partials/latest-blog' );
						break;

					case 'content_row':
						get_template_part( 'partials/row-content' );
						break;

					case 'callout_row':
						get_template_part( 'partials/row-callout' );
						break;

					case 'three_columns_row':
						get_template_part( 'partials/row-three-columns' );
						break;

					case 'related_links':
						get_template_part( 'partials/related-links' );
						break;
				}
			}
		}

		if ( $section_bg == 'image' ) { ?>
			</div>
		<?php }  ?>
		</section>
	<?php }
}
