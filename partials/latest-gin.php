<?php $args = array(
	'post_type' => 'gins',
	'posts_per_page' => 1
);

$latest_gin_query = new WP_Query( $args );
if ( $latest_gin_query->have_posts() ) {
	while ( $latest_gin_query->have_posts() ) {
		$latest_gin_query->the_post(); ?>
		<div class="row latest-gin">
			<div class="small-12 columns">
				<div class="bordered-container">
					<svg class="flourish top-left"><use xlink:href="#top-left-corner" /></svg>
					<div class="row large-collapse">
						<div class="small-12 large-10 large-centered columns">
							<div class="row" data-equalizer data-equalize-on="large">
								<div class="small-12 large-4 large-push-8 columns">
									<div class="gin-bottle-container" data-equalizer-watch>
										<img class="gin-bottle-main" src="<?php echo get_field( 'gin_bottle' )['url'] ?>">
									</div>
								</div>
								<div class="small-12 large-7 large-pull-5 columns" data-equalizer-watch>
									<!-- <p class="secondary-color"><strong><em>Our members are currently enjoying:</em></strong></p> -->
									<p class="secondary-color"><strong><em>Our members are currently enjoying:</em></strong></p>
									<?php echo apply_filters( 'the_content', get_field( 'introduction_content' ) ); ?>
									<a href="<?php the_permalink() ?>" class="button">Read More</a>
								</div>
							</div>
						</div>
					</div>
					<svg class="flourish bottom-right"><use xlink:href="#bottom-right-corner" /></svg>
				</div>
			</div>
		</div>
	<?php }
	wp_reset_postdata();
} else { ?>
	<div class="row latest-gin">
		<div class="small-12 columns">
			<div class="bordered-container">
				<svg class="flourish top-left"><use xlink:href="#top-left-corner" /></svg>
				<div class="row large-collapse">
					<div class="small-12 large-10 large-centered columns">
						<div class="row" data-equalizer data-equalize-on="large">
							<div class="small-12 large-4 large-push-8 columns">
								<div class="gin-bottle-container" data-equalizer-watch>
									<img class="gin-bottle-main" src="<?php echo TEMPLATEURI . 'images/mystery-bottle.png'; ?>">
								</div>
							</div>
							<div class="small-12 large-7 large-pull-5 columns" data-equalizer-watch>
								<?php echo apply_filters( 'the_content', get_field( 'default_coming_soon', 'option' ) ); ?>
							</div>
						</div>
					</div>
				</div>
				<svg class="flourish bottom-right"><use xlink:href="#bottom-right-corner" /></svg>
			</div>
		</div>
	</div>
<?php } ?>
