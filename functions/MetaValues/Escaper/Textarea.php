<?php

namespace ES\Escaper;

class Textarea extends EscaperAbstract
{
    public function handle($value)
    {
        return $this->kses(nl2br($value));
    }
}
