<div class="row content-row">
	<div class="small-12 large-4 columns">
		<?php echo apply_filters('the_content', get_sub_field( 'left_column' ) ); ?>
	</div>
	<div class="small-12 large-4 columns">
		<?php echo apply_filters('the_content', get_sub_field( 'middle_column' ) ); ?>
	</div>
	<div class="small-12 large-4 columns">
		<?php echo apply_filters('the_content', get_sub_field( 'right_column' ) ); ?>
	</div>
</div>