<div class="row content-row">
	<div class="small-12 columns">
		<?php echo apply_filters('the_content', get_sub_field( 'row_content' ) ); ?>
	</div>
</div>