<?php
/*
 * File name - metaboxes.php
 * Meta box functionality has been created by Rilwis @ http://www.deluxeblogtips.com and can be downloaded as a plugin from the wordpress repository.
 * Use underscore (_) at the beginning to make keys hidden, for example $prefix = '_es_';
 */
$meta_boxes = array();
$prefix = '_es_';
$dir = 'metaboxes/';

//require_once( $dir . '/example.php' );

/**
 * Register meta boxes
 *
 * @return void
 */
if ( ! function_exists( 'rw_register_meta_boxes' ) ) {
	function rw_register_meta_boxes() {
		global $meta_boxes;
		if ( class_exists( 'RW_Meta_Box' ) ) {
			foreach ( $meta_boxes as $meta_box ) {
				new RW_Meta_Box( $meta_box );
			}
		}
	}

	add_action( 'admin_init', 'rw_register_meta_boxes' );
}

