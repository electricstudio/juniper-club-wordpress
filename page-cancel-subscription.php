<?php if ( !is_user_logged_in() ) {
	auth_redirect();
}

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		get_template_part('templates/page-header');

		get_template_part('templates/cancel-form');
	}
}
get_footer();

