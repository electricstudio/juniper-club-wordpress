<div class="row newsletter-form">
	<div class="small-12 medium-10 large-8 medium-centered columns">

	<h3>Sign Up for our Newsletter</h3>

	<form action="//juniperclub.us13.list-manage.com/subscribe/post?u=98d1a9be50f574c38bbd3e14e&amp;id=9813b3423e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		<div id="mc_embed_signup_scroll">
			<div class="row">
				<div class="small-12 medium-8 columns">
					<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Email Address" required>
				</div>
				<div class="small-12 medium-4 columns">
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_98d1a9be50f574c38bbd3e14e_9813b3423e" tabindex="-1" value=""></div>
					<input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="button">
				</div>
			</div>
		</div>
	</form>

	</div>
</div>