<section class="page-section white form-page" id="form-page-1">
	<div class="row">
		<div class="small-12 medium-9 medium-centered large-6 columns">
			<form class="subscription-form" action="subscription-form-page-1" method="post">
				<?php wp_nonce_field( 'es_ajax_form', 'es_ajax_form' ); ?>
				<h3>Enter your Email address to get started</h3>
				<ul class="sign-up-form">
					<?php $args = array(
						array(
							'label'		  => 'Email Address',
							'type'		  => 'email',
							'class'		  => 'email-address',
							'data-stripe' => 'email-address',
						),
						array(
							'label'		  => 'Password',
							'type'		  => 'password',
							'class'		  => 'password',
							'data-stripe' => 'password',
						),
						array(
							'label'		  => 'Confirm Password',
							'type'		  => 'password',
							'class'		  => 'confirm-password',
							'data-stripe' => 'confirm-password',
						),
						array(
							'label'		  => 'Create Account',
							'type'		  => 'submit',
							'class'		  => 'create-account',
							'data-stripe' => 'create-account',
						),
						array(
							'label'		  => '1',
							'type'		  => 'hidden',
							'class'		  => 'form-page-number',
							'data-stripe' => 'form-page-number',
						),
					);

					echo es_form_builder ( $args ) ?>
				</ul>
			</form>
		</div>
	</div>
</section>