<?php $args = array(
	'post_type' => 'post',
	'posts_per_page' => 1
);

$latest_post_query = new WP_Query( $args );
if ( $latest_post_query->have_posts() ) {
	while ( $latest_post_query->have_posts() ) {
		$latest_post_query->the_post();
		get_template_part( 'partials/blog-post' );
	}
	wp_reset_postdata();
} ?>