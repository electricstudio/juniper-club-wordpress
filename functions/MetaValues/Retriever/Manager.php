<?php

namespace ES\Retriever;

use Exception;

class Manager extends RetrieverManagerAbstract implements RetrieverManagerInterface
{
    protected $prefix;
    protected $response;
    protected $post_id;
    protected $post_custom_array;
    protected $key;
    protected $data;
    protected $meta_keys;
    protected $meta_key;
    protected $meta_value;
    protected $expiration;
    protected $cacheKey;
    protected $retreiver;
    protected $multiples = [
        'Images',
        'Attachments',
        'Set',
    ];

    public function manage(array $meta_keys, $pid = null, $prefix = '_es_')
    {
        $this->setMetaKeys($meta_keys);
        $this->setPrefix($prefix);
        $this->setPostId($pid);
        $this->setCacheKey();

        $response = $this->cache->get($this->cacheKey, function () {
            return $this->getResponse();
        });

        return $response;
    }

    protected function getResponse()
    {
        foreach ($this->meta_keys as $key => $data) {
            if (! array_key_exists(0, $data) or ! $data) {
                throw new Exception('Meta Keys must have options.');
            }

            if (! array_key_exists(1, $data)) {
                $data[1] = 'raw';
            }

            $retreiver_name = str_replace(' ', '', ucwords(str_replace('_', ' ', $data[1])));//test
            $meta_key       = $this->prefix.$data[0];

            $this->setRetreiver($retreiver_name);
            $this->setMetaValue($retreiver_name, $meta_key);
            $this->setKey($key);
            $this->setData($data);
            $this->callRetreiver();
        }

        return $this->response;
    }

    protected function callRetreiver()
    {
        $this->response[$this->key] = (new $this->retreiver)->handle($this->meta_value, $this->data);
    }

    protected function setRetreiver($retreiver_name)
    {
        $this->retreiver  = __NAMESPACE__ . '\\'. $retreiver_name;
    }

    protected function setMetaValue($retreiver_name, $meta_key)
    {
        $this->meta_value = $this->getPostMeta($retreiver_name, $meta_key);
    }

    protected function setMetaKeys(array $meta_keys)
    {
        $this->meta_keys = $meta_keys;
    }

    protected function setData($data)
    {
        $this->data = $data;
    }

    protected function setKey($key)
    {
        $this->key = $key;
    }

    protected function setCacheKey()
    {
        $this->cacheKey = $this->cache->keyForMeta($this->meta_keys, $this->post_id);
    }

    protected function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    protected function setPostId($post_id)
    {
        $this->post_id = $post_id ?: $this->getPostId($post_id);
    }
}
