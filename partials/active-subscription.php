<div class="row">
	<div class="small-12 medium-9 medium-centered large-6 columns">
		<div class="bordered-container subscription-round-up">
			<?php $date = strtotime(get_field('sub_subscription_date'));
			$date_formatted = date('jS F Y', $date);
			switch ( get_field( 'sub_subscription_type' ) ) {
				case 'monthly':
					if( get_field( 'sub_subscription_method' ) == 'gift' ) { ?>
						<h3><strong>The Juniper Club Monthly Bottle Gift</strong></h3>
						<h1>£400.00</h1>
						<p>Your Recipient will receive a bottle every month starting <?php echo $date_formatted ?></p>
					<?php } else { ?>
						<h3><strong>The Juniper Club Monthly Membership</strong></h3>
						<h1>£35.00</h1>
						<p>Recurs every month starting <?php echo $date_formatted ?> - Cancel at any time</p>
					<?php }
					break;

				case 'bi-monthly':
					if( get_field( 'sub_subscription_method' ) == 'gift' ) { ?>
						<h3><strong>The Juniper Club Bi-Monthly Gift</strong></h3>
						<h1>£205.00</h1>
						<p>Your Recipient will receive a bottle every other month starting <?php echo $date_formatted ?></p>
					<?php } else { ?>
						<h3><strong>The Juniper Club Bi-Monthly Membership</strong></h3>
						<h1>£35.00</h1>
						<p>Recurs every other month starting <?php echo $date_formatted ?> - Cancel at any time</p>
					<?php }
					break;

				case 'quarterly':
					if( get_field( 'sub_subscription_method' ) == 'gift' ) { ?>
						<h3><strong>The Juniper Club Quarterly Gift</strong></h3>
						<h1>£135.00</h1>
						<p>Your Recipient will receive a bottle every 3 months starting <?php echo $date_formatted ?></p>
					<?php } else { ?>
						<h3><strong>The Juniper Club Quarterly Membership</strong></h3>
						<h1>£35.00</h1>
						<p>Recurs every 3 months starting <?php echo $date_formatted ?> - Cancel at any time</p>
					<?php }
					break;
			}?>
		</div>
	</div>
</div>
<div class="row">
	<?php switch ( get_field( 'sub_subscription_method' ) ) {
		case 'gift': ?>
			<div class="small-12 columns text-center">
				<h2>Delivery Details</h2>
				<ul class="delivery-details">
					<li><?php the_field( 'sub_recipient_first_name' ) ?> <?php the_field( 'sub_recipient_last_name' )?></li>
					<li><?php the_field( 'sub_recipient_address_1' ) ?></li>
					<?php if( get_field( 'sub_recipient_address_2' ) ) { ?>
						<li><?php the_field( 'sub_recipient_address_2' ) ?></li>
					<?php } ?>
					<li><?php the_field( 'sub_recipient_city' ) ?></li>
					<li><?php the_field( 'sub_recipient_county' ) ?></li>
					<li><?php the_field( 'sub_recipient_postcode' ) ?></li>
				</ul>
			</div>
			<?php break;

		default: ?>
			<div class="small-12 columns text-center">
				<h2>Address Details</h2>
				<ul class="delivery-details">
					<li><?php the_field( 'sub_first_name' ) ?> <?php the_field( 'sub_last_name' )?></li>
					<li><?php the_field( 'sub_delivery_address_1' ) ?></li>
					<?php if( get_field( 'sub_delivery_address_2' ) ) { ?>
						<li><?php the_field( 'sub_delivery_address_2' ) ?></li>
					<?php } ?>
					<li><?php the_field( 'sub_delivery_city' ) ?></li>
					<li><?php the_field( 'sub_delivery_county' ) ?></li>
					<li><?php the_field( 'sub_delivery_postcode' ) ?></li>
					<li>Contact Number: <?php the_field( 'sub_contact_phone' ) ?></li>
				</ul>
			</div>
			<?php break;
	} ?>
</div>