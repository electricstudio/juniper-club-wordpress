<section class="page-section patterned">
	<div class="row">
		<div class="small-12 text-center">
			<?php if ( is_category() ) {
				$title = single_cat_title( 'Posts in Category ', false );
			} elseif ( is_author() ) {
				$title = 'Posts by ' . get_the_author();
			} elseif ( is_home() ) {
				$title = 'Blog';
			} else {
				$title = get_the_title();
			} ?>
			<h1><?php echo apply_filters( 'the_title' , esc_html( $title ) ); ?></h1>
		</div>
	</div>
</section>