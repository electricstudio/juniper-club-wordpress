<?php get_header();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		get_template_part('templates/page-header');

		get_template_part('templates/page-builder');
	}
}
get_footer();

