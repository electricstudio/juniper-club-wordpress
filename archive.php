<?php get_header();
get_template_part('templates/page-header');
if ( have_posts() ) {
	while ( have_posts() ) { ?>
		<section class="page-section white">
			the_post(); ?>
			get_template_part( 'partials/blog-post' );
		</section>
	<?php }
}
get_footer();

