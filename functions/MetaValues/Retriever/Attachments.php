<?php

namespace ES\Retriever;

class Attachments
{
    public function handle(array $value)
    {
        $attachment_array = [];

        foreach ($value as $attachment_id) {
            $attachment_array[] = wp_get_attachment_url(intval($attachment_id));
        }

        return $attachment_array;
    }
}
