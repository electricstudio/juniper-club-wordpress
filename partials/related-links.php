<div class="row">
	<?php if ( have_rows( 'page_links' ) ) {
		while ( have_rows( 'page_links' ) ) {
			the_row();
			$page_object = get_sub_field( 'page_link' ); ?>
			<div class="small-12 large-6 columns">
				<div class="bordered-container text-left">
					<svg class="flourish top-left"><use xlink:href="#top-left-corner" /></svg>
					<div class="row large-collapse">
						<div class="small-12 large-10 large-centered columns">
							<h2><?php echo esc_html( $page_object->post_title ) ?></h2>
							<p><?php echo esc_html( $page_object->post_excerpt ); ?></p>
							<a class="button" href="<?php echo get_permalink( $page_object->ID ) ?>">Read More</a>
						</div>
					</div>
					<svg class="flourish bottom-right"><use xlink:href="#bottom-right-corner" /></svg>
				</div>
			</div>
		<?php }
	} ?>
</div>