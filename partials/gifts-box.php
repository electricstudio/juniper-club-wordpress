<div class="row large-collapse subscription-section">
	<div class="small-12 large-4 columns">
		<div class="subscription-method" id="#subscription_box">
			<div class="row">
				<div class="small-12 medium-4 large-12 columns">
					<div class="subscription-box">
						<svg class="flourish top-left"><use xlink:href="#sub-box-top-left" /></svg>
						<svg class="flourish top-right"><use xlink:href="#sub-box-top-right" /></svg>
						<svg class="subscription-method-icon"><use xlink:href="#bottles-monthly" /></svg>
						<svg class="flourish bottom-left"><use xlink:href="#sub-box-bottom-left" /></svg>
						<svg class="flourish bottom-right"><use xlink:href="#sub-box-bottom-right" /></svg>
					</div>
				</div>
				<div class="small-12 medium-8 large-12 columns">
					<div class="subscription-details">
						<h2>Monthly</h2>
						<p class="secondary-color"><strong><em>A unique gin delivery every month for a whole year (12 gin deliveries)</em></strong></p>
						<a class="button block" href="<?php echo get_permalink( get_page_by_path( 'join-now' ) ) ?>?sub_method=gift&sub_type=monthly"><strong>£400</strong> for 1 bottle <strong>every Month</strong></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="small-12 large-4 columns">
		<div class="subscription-method">
			<div class="row">
				<div class="small-12 medium-4 large-12 columns">
					<div class="subscription-box">
						<svg class="flourish top-left"><use xlink:href="#sub-box-top-left" /></svg>
						<svg class="flourish top-right"><use xlink:href="#sub-box-top-right" /></svg>
						<svg class="subscription-method-icon"><use xlink:href="#bottles-bi-monthly" /></svg>
						<svg class="flourish bottom-left"><use xlink:href="#sub-box-bottom-left" /></svg>
						<svg class="flourish bottom-right"><use xlink:href="#sub-box-bottom-right" /></svg>
					</div>
				</div>
				<div class="small-12 medium-8 large-12 columns">
					<div class="subscription-details">
						<h2>Bi-Monthly</h2>
						<p class="secondary-color"><strong><em>A unique gin delivery every other month for a whole year (6 gin deliveries)</em></strong></p>
						<a class="button block" href="<?php echo get_permalink( get_page_by_path( 'join-now' ) ) ?>?sub_method=gift&sub_type=bi-monthly"><strong>£205</strong> for 1 bottle <strong>every 2 Months</strong></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="small-12 large-4 columns">
		<div class="subscription-method">
			<div class="row">
				<div class="small-12 medium-4 large-12 columns">
					<div class="subscription-box">
						<svg class="flourish top-left"><use xlink:href="#sub-box-top-left" /></svg>
						<svg class="flourish top-right"><use xlink:href="#sub-box-top-right" /></svg>
						<svg class="subscription-method-icon"><use xlink:href="#bottles-quarterly" /></svg>
						<svg class="flourish bottom-left"><use xlink:href="#sub-box-bottom-left" /></svg>
						<svg class="flourish bottom-right"><use xlink:href="#sub-box-bottom-right" /></svg>
					</div>
				</div>
				<div class="small-12 medium-8 large-12 columns">
					<div class="subscription-details">
						<h2>Quarterly</h2>
						<p class="secondary-color"><strong><em>A unique gin delivery every three months for a whole year (4 gin deliveries)</em></strong></p>
						<a class="button block" href="<?php echo get_permalink( get_page_by_path( 'join-now' ) ) ?>?sub_method=gift&sub_type=quarterly"><strong>£135</strong> for 1 bottle <strong>every 3 Months</strong></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
