<?php get_header(); ?>
<section class="page-section patterned">
	<div class="row">
		<div class="small-12 text-center">
			<h1>404: Not found</h1>
			<h2>The page you're looking for is not here.</h2>
		</div>
	</div>
</section>
<?php get_footer();
