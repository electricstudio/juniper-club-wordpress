<?php
$socials = apply_filters(
	'es_social_links',
	array(
		'google',
		'twitter',
		'linkedin',
		'facebook',
	)
);

if ( is_array( $socials ) ) {
	?>
	<ul class="list--inline list--social">
		<?php
		foreach ( $socials as $social ) {
			$url = get_option( 'es_' . $social );
				if ( ! empty( $url ) ) {
				?>
				<li>
					<a target="_blank" href="<?php echo esc_attr( $url ); ?>" class="social social--<?php echo esc_attr( $social ); ?>">
						<svg>
							<use xlink:href="#social_<?php echo esc_attr( $social ); ?>" />
						</svg>
					</a>
				</li>
				<?php
			}
		}
		?>
	</ul>
	<?php
}
