<div class="column">
	<a href="<?php the_permalink();?>">
		<div class="bordered-container gin-archive">
			<div class="row">
				<div class="small-12 text-center">
					<img class="gin-bottle-single" src="<?php echo the_post_thumbnail_url( 'full' ); ?>">
					<p class="secondary-color"><strong><em><?php the_time('F Y') ?></em></strong></p>
					<?php the_title( '<h3>', '</h3>' ); ?>
				</div>
			</div>
		</div>
	</a>
</div>