		<section class="page-section purple">
			<?php get_template_part( 'partials/newsletter-signup' ); ?>
		</section>
		<footer class="main-footer">
			<?php $bg_image = get_field( 'footer_background_image', 'option' ); ?>
			<section class="page-section image" style="background: url('<?php echo $bg_image['sizes']['featured-img'] ?>') center no-repeat; background-size: cover;">
				<div class="overlay">
					<ul class="social">
						<li><a href="<?php echo get_field( 'facebook_url', 'option' ); ?>"><svg><use xlink:href="#social_facebook" /></svg></a></li>
						<li><a href="<?php echo get_field( 'twitter_url', 'option' ); ?>"><svg><use xlink:href="#social_twitter" /></svg></a></li>
						<li><a href="<?php echo get_field( 'instagram_url', 'option' ); ?>"><svg><use xlink:href="#social_instagram" /></svg></a></li>
					</ul>
					<?php if ( has_nav_menu( 'footer' ) ) {
						wp_nav_menu( array( 'theme_location'  => 'footer', ) );
					} ?>
					<p>&copy; <?php echo esc_attr( date( 'Y' ) ); ?> Juniper Club Ltd. All rights reserved.</p>
					<p><a href="https://www.electricstudio.co.uk/web-design-oxford" title="Digital Agency Oxford" class="es" target="_blank">Web Design Oxford <svg class="es-logo"><use xlink:href="#es" /></svg></a></p>
				</div>
			</section>
		</footer>
		<div class="mobile-menu-overlay">
			<?php if ( has_nav_menu( 'mobile' ) ) {
				wp_nav_menu( array( 'theme_location'  => 'mobile', ) );
			} ?>
		</div>
		<?php wp_footer(); ?>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-69023930-4', 'auto');
			ga('send', 'pageview');

		</script>
	</body>
	</html>
