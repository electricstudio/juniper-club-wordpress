/* global jQuery: false; ES_CONFIG: false; Stripe: false; */
(function ($) {
	'use strict';

	$(document).ready(function () {
		$(document).foundation();

		var App = {
			init: function () {

				var current_page = $('#form-page-number').val();

				$('.form-header').addClass('form-page-' + current_page );

				$('.menu-button').click( function () {
					$('.mobile-menu-overlay').addClass('toggled');
				});

				$('.mobile-menu-overlay').click( function (e) {
					e.stopPropagation();
					$(this).removeClass('toggled');
				});

				App.svg.resize('.subscription-method-icon');

				$(window).resize( function () {
					App.svg.resize('.subscription-method-icon');
				});

				App.sub_form.labels();
				App.sub_form.subscription_box.init();

				$('.subscription_method').change( function () {
					App.sub_form.subscription_box.change();
				});

				$('button').click(function (event) {
					if( $(this).val() !== 'update-details' && $(this).val() !== 'confirm-cancellation' && $(this).val() !== 'update-card-details') {
						event.preventDefault();
						if( $(this).attr('data-value') ) {
							App.sub_form.ajax(current_page, $(this).attr('data-value') );
						} else {
							App.sub_form.ajax(current_page);
						}
					}
				});
			},
			svg: {
				resize: function (selector) {
					$(selector).each( function () {
						$(this).css( 'height', $(this).width() );
					});
				}
			},
			sub_form: {
				labels: function () {
					$('input').each( function () {
						if( $(this).val() ) {
							$(this).prev('label').css('display', 'none');
						}
					})

					$('input').focusin( function () {
						if( !$(this).is(':checkbox') ) {
							$(this).prev('label').css('display', 'none');
						}
					});

					$('input').focusout( function () {
						if( !$(this).is(':checkbox') ) {
							if( !$(this).val() ) {
								$(this).prev('label').css('display', '');
							}
						}
					});

					$('textarea').focusin( function () {
						if( !$(this).is(':checkbox') ) {
							$(this).prev('label').css('display', 'none');
						}
					});

					$('textarea').focusout( function () {
						if( !$(this).is(':checkbox') ) {
							if( !$(this).val() ) {
								$(this).prev('label').css('display', '');
							}
						}
					});

					$('label').click( function () {
						if( $(this).next('input').length ) {
							$(this).css('display', 'none');
							$(this).next('input').focus();
						}

						if( $(this).next('textarea').length ) {
							$(this).css('display', 'none');
							$(this).next('textarea').focus();
						}
					});
				},
				subscription_box : {
					init: function () {
						$('.subscription_method').find('option').each( function () {
							if( $(this).is(':selected') ) {
								if( $(this).val() == 'gift' ) {
									$('.gift-toggle').css('display', 'block');
									$('.subscription-toggle').css('display', 'none');
									$('.sign-up-form').addClass('is-gift');
									$('.sign-up-form').removeClass('is-subscription');
								} else {
									$('.gift-toggle').css('display', 'none');
									$('.subscription-toggle').css('display', 'block');
									$('.sign-up-form').removeClass('is-gift');
									$('.sign-up-form').addClass('is-subscription');
								}
							}
						});
					},
					change: function () {
						if( $('.subscription_method').val() == 'gift' ) {
							$('.gift-toggle').css('display', 'block');
							$('.subscription-toggle').css('display', 'none');
							$('.sign-up-form').addClass('is-gift');
							$('.sign-up-form').removeClass('is-subscription');
						} else {
							$('.gift-toggle').css('display', 'none');
							$('.subscription-toggle').css('display', 'block');
							$('.sign-up-form').removeClass('is-gift');
							$('.sign-up-form').addClass('is-subscription');
						}
					}
				},
				ajax: function (page_number, subscription_type) {

					var results_container = $('.ajax-container'),
						ajax_args = {
							url: ES_CONFIG.admin_url,
							type: 'POST',
							nonce: $('#es_ajax_form').val(),
							dataType: 'html'
						},
						$form = $('.subscription-form');

					if (page_number == 1) {
						ajax_args['data'] = {
							action: 'es_ajax_form',
							email_address: $('#email-address').val(),
							password: $('#password').val(),
							confirm_password: $('#confirm-password').val(),
							page_number: page_number
						};

						if( $('#subscription_type').length ) {
							ajax_args['data']['subscription_type'] =  $('#subscription_type').val();
						}

						if( $('#sub_method').length ) {
							ajax_args['data']['subscription_method'] =  $('#sub_method').val();
						}
					}

					if (page_number == 2) {
						ajax_args['data'] = {
							action: 'es_ajax_form',
							user_id: $('#user_id').val(),
							subscription_type: subscription_type,
							page_number: page_number
						};

						if( $('#sub_method').length ) {
							ajax_args['data']['subscription_method'] =  $('#sub_method').val();
						}
					}

					if (page_number == 3) {
						Stripe.card.createToken($form, response_handler);
					}

					if (page_number == 4) {
						ajax_args['data'] = {
							action: 'es_ajax_form',
							user_id: $('input[name="user_id"]').val(),
							subscription_method: $('.subscription_method').val(),
							subscription_type: $('input[name="subscription_type"]').val(),
							first_name: $('input[data-stripe="first-name"]').val(),
							last_name: $('input[data-stripe="last-name"]').val(),
							contact_phone: $('input[data-stripe="contact-phone"]').val(),
							delivery_address_1: $('input[data-stripe="delivery-street-address"]').val(),
							delivery_address_2: $('input[data-stripe="delivery-address-line-2"]').val(),
							delivery_city: $('input[data-stripe="delivery-city"]').val(),
							delivery_county: $('input[data-stripe="delivery-county"]').val(),
							delivery_postcode: $('input[data-stripe="delivery-postcode"]').val(),
							discount_code: $('input[data-stripe="discount-code"]').val(),
							str_token: $('input[name="stripeToken"]').val(),
							page_number: page_number
						};

						if( $('.subscription_method').val() == 'gift' ) {
							ajax_args['data']['recipient_first_name'] = $('input[data-stripe="recipient-first-name"]').val(),
							ajax_args['data']['recipient_last_name'] = $('input[data-stripe="recipient-last-name"]').val(),
							ajax_args['data']['recipient_address_1'] = $('input[data-stripe="recipient-street-address"]').val(),
							ajax_args['data']['recipient_address_2'] = $('input[data-stripe="recipient-address-line-2"]').val(),
							ajax_args['data']['recipient_city'] = $('input[data-stripe="recipient-city"]').val(),
							ajax_args['data']['recipient_county'] = $('input[data-stripe="recipient-county"]').val(),
							ajax_args['data']['recipient_postcode'] = $('input[data-stripe="recipient-postcode"]').val(),
							ajax_args['data']['recipient_email'] = $('input[data-stripe="recipient-email-address"]').val(),
							ajax_args['data']['recipient_start_month'] = $('select[data-stripe="recipient-start-month"]').val(),
							ajax_args['data']['recipient_message'] = $('textarea[data-stripe="recipient-message"]').val();
						}
					}

					if (page_number != 3) {
						$(".ajax-loading-image").show();
						$.ajax(
							ajax_args
						).done(function(ret) {
							$(".ajax-loading-image").hide();
							results_container.html(ret);
							var current_page = $('#form-page-number').val();

							$('.form-header').addClass('form-page-' + current_page );

							$('.button').on('click', function (event) {
								event.preventDefault();
								if( $(this).attr('data-value') ) {
									App.sub_form.ajax(current_page, $(this).attr('data-value') );
								} else {
									App.sub_form.ajax(current_page);
								}
							});

							if( page_number ) {
								App.svg.resize('.subscription-method-icon');
							}

							App.sub_form.labels();
							App.sub_form.subscription_box.init();

							$('.subscription_method').change( function () {
								App.sub_form.subscription_box.change();
							});
						});
					}
				}
			}
		};

		App.init();


		function response_handler (status, response) {
			var $form = $('.subscription-form');

			if (response.error) {
				$form.find('.payment-errors').text(response.error.message);
			} else {
				// Get the token ID:
				var token = response.id;

				// Insert the token ID into the form so it gets submitted to the server:
				$form.append($('<input type="hidden" name="stripeToken">').val(token));

				App.sub_form.ajax(4);
			}
		}

	});
}(jQuery));
