
<?php if( $_POST ) {

	$req = $_POST;

	$customer = \Stripe\Customer::retrieve( $req['customer_id'] );
	$customer->delete();

	send_cancelled_email ( $req['subscription-post-id'] );

	wp_trash_post( $req['subscription-post-id'] ); ?>
	<section class="page-section white form-page" id="form-page-3">
		<div class="row">
			<div class="small-12 medium-9 medium-centered large-6 columns">
				<h2>Your subscription has been cancelled</h2>
				<?php $account_page = get_page_by_path( 'my-account' ); ?>
				<a href="<?php echo get_permalink( $account_page->ID ) ?>" class="button">Back to My Account</a>
			</div>
		</div>
	</section>
<?php } else { ?>
	<section class="page-section white form-page" id="form-page-3">
		<div class="row">
			<div class="small-12 medium-9 medium-centered large-6 columns">
				<form class="subscription-form" method="post">
					<ul class="sign-up-form">
						<?php
							$subscription_obj = get_subscription_by_cust_id( $_GET['cust_id'] );
							$subscription_details = get_post_custom( $subscription_obj[0]->ID );

							$args = array(
								array(
									'label' => $subscription_details['sub_customer_id'][0],
									'type'	=> 'hidden',
									'class' => 'customer_id',
									'data-stripe' => 'customer-id',
									'value' => '',
								),
								array(
									'label' => $subscription_obj[0]->ID,
									'type'	=> 'hidden',
									'class' => 'subscription-post-id',
									'data-stripe' => 'subscription-post-id',
									'value' => '',
								),
								array(
									'label' => 'Confirm Cancellation',
									'type'	=> 'submit',
									'class' => 'confirm-cancellation',
									'data-stripe' => 'confirm-cancellation',
									'value' => '',
								),
							);

						echo es_form_builder ( $args );?>
					</ul>
				</form>
			</div>
		</div>
	</section>
<?php } ?>
