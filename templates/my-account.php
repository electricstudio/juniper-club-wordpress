<?php $subscriptions_args = array(
	'post_type' => 'subscriptions',
	'author' => get_current_user_id()
);

$subscriptions_query = new WP_Query( $subscriptions_args );

if ( $subscriptions_query->have_posts() ) {
	while ( $subscriptions_query->have_posts() ) {
		$subscriptions_query->the_post();

		$cutoff_date = get_next_cutoff();

		$date = strtotime( get_field( 'sub_subscription_date' ) );
		$date_formatted = date( 'jS F Y', $date ); ?>
		<section class="page-section white">
			<a href="<?php the_permalink() ?>">
				<div class="row">
					<div class="small-12 columns">
						<div class="bordered-container">
							<svg class="flourish top-left"><use xlink:href="#top-left-corner" /></svg>
							<div class="row large-collapse">
								<?php if ( get_field( 'sub_subscription_method' ) == 'gift' ) { ?>
									<div class="small-12 large-10 large-centered columns text-center">
										<h3>Your <?php echo ucfirst( get_field( 'sub_subscription_type' ) ) ?> gift bottle for <?php echo get_field( 'sub_recipient_first_name' ) ?></h3>
										<p>First bottle sent out: <?php echo $date_formatted ?></p>
									</div>
								<?php } else { ?>
									<div class="small-12 large-10 large-centered columns text-center">
										<h3>Your <?php echo ucfirst( get_field( 'sub_subscription_type' ) ) ?> gin subscription</h3>
										<p>Subscription valid from: <?php echo $date_formatted ?></p>
									</div>
								<?php } ?>
							</div>
							<svg class="flourish bottom-right"><use xlink:href="#bottom-right-corner" /></svg>
						</div>
					</div>
				</div>
			</a>
		</section>
	<?php }
	wp_reset_postdata();
} else { ?>
	<section class="page-section white">
		<div class="row">
			<div class="small-12 text-center">
				<h2>No currently active subscriptions - Get started</h2>
			</div>
		</div>
	</section>
	<section class="page-section patterned">
		<?php get_template_part( 'partials/subscriptions-box' ); ?>
	</section>
<?php } ?>
