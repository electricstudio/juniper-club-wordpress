<?php
/*
 * File name - nav.php
 * Register the navigation areas.
 */
register_nav_menus(
	array(
		'primary' => 'Primary Navigation',
		'footer' => 'Footer Navigation',
		'mobile' => 'Mobile Navigation'
	)
);


function home_link_logo ( $item_output, $item, $depth, $args ) {
	if ( in_array( 'menu-item-home', $item->classes ) ) {
		$item_output = '<a href="' . $item->url . '"><img src="' . TEMPLATEURI . '/images/svg/logo.svg"></a>';
	}

	return $item_output;
}

add_filter( 'walker_nav_menu_start_el', 'home_link_logo', 9999, 4);