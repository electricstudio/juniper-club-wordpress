<?php

namespace ES\Retriever;

class Title
{
    public function handle($value)
    {
        return apply_filters('the_title', $value);
    }
}
