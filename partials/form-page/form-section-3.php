<script type="text/javascript">
  // Stripe.setPublishableKey( 'pk_test_svXEpxRSxRBTaPD96znvu5SZ' );
  Stripe.setPublishableKey( 'pk_live_EQG1wHsWup0FHqodZvgLzruT' );
</script>
<section class="page-section white form-page" id="form-page-3">
	<div class="row">
		<div class="small-12 medium-9 medium-centered large-6 columns">
			<form class="subscription-form" action="subscription-form-page-3" method="post">
				<?php wp_nonce_field( 'es_ajax_form', 'es_ajax_form' );
				if ( array_key_exists( 'sub_method', $_GET ) ) {
					$sub_method = $_GET['sub_method'];
				} else if ( array_key_exists( 'subscription_method', $_REQUEST ) ) {
					$sub_method = $_REQUEST['subscription_method'];
				} else {
					$sub_method = false;
				} ?>
				<select name="subscription_method" class="subscription_method">
					<option value="subscription">Subscription</option>
					<option value="gift" <?php if ( $sub_method == 'gift' ) { ?> selected="selected" <?php }?>>Gift</option>
				</select>

				<div class="row">
					<div class="small-12 columns">
						<div class="bordered-container subscription-round-up">
							<?php $req = array_map( 'esc_attr', $_REQUEST );
							$date_formatted = date( 'jS F Y', get_next_cutoff() );
							switch ( $req['subscription_type'] ) {
								case 'monthly': ?>
									<div class="subscription-toggle">
										<h3><strong>The Juniper Club Monthly Membership</strong></h3>
										<h1>£35.00</h1>
										<p>Recurs every month starting <?php echo $date_formatted ?> - Cancel at any time</p>
									</div>
									<div class="gift-toggle">
										<h3><strong>The Juniper Club Monthly Gift</strong></h3>
										<h1>£400.00</h1>
										<p>Your gift recipient will receive a bottle of gin every month for 12 months</p>
									</div>
								<?php break;
								case 'bi-monthly': ?>
									<div class="subscription-toggle">
										<h3><strong>The Juniper Club Bi-Monthly Membership</strong></h3>
										<h1>£35.00</h1>
										<p>Recurs every other month starting <?php echo $date_formatted ?> - Cancel at any time</p>
									</div>
									<div class="gift-toggle">
										<h3><strong>The Juniper Club Bi-Monthly Gift</strong></h3>
										<h1>£205.00</h1>
										<p>Your gift recipient will receive a bottle of gin every other month for 12 months</p>
									</div>
								<?php break;
								case 'quarterly': ?>
									<div class="subscription-toggle">
										<h3><strong>The Juniper Club Quarterly Membership</strong></h3>
										<h1>£35.00</h1>
										<p>Recurs every 3 months starting <?php echo $date_formatted ?> - Cancel at any time</p>
									</div>
									<div class="gift-toggle">
										<h3><strong>The Juniper Club Quarterly Gift</strong></h3>
										<h1>£135.00</h1>
										<p>Your gift recipient will receive a bottle of gin every 3 months for 12 months</p>
									</div>
								<?php break;
							} ?>
						</div>
					</div>
				</div>

				<ul class="sign-up-form">
					<?php $args = array(
						array(
							'label' => 'First Name',
							'type'	=> 'text',
							'class' => 'first-name',
							'data-stripe' => 'first-name',
							'value' => '',
						),
						array(
							'label' => 'Last Name',
							'type'	=> 'text',
							'class' => 'last-name',
							'data-stripe' => 'last-name',
							'value' => '',
						),
						array(
							'label' => 'Contact Phone Number',
							'type'	=> 'text',
							'class' => 'contact-phone',
							'data-stripe' => 'contact-phone',
							'value' => '',
						),
						array(
							'label' => 'Billing Details',
							'type'	=> 'section',
							'class' => 'delivery-address',
							'data-stripe' => 'delivery-address',
						),
						array(
							'label' => 'Street Address',
							'type'	=> 'text',
							'class' => 'delivery-street-address',
							'data-stripe' => 'delivery-street-address',
							'value' => '',
						),
						array(
							'label' => 'Address Line 2',
							'type'	=> 'text',
							'class' => 'delivery-address-line-2',
							'data-stripe' => 'delivery-address-line-2',
							'value' => '',
						),
						array(
							'label' => 'City',
							'type'	=> 'text',
							'class' => 'delivery-city',
							'data-stripe' => 'delivery-city',
							'value' => '',
						),
						array(
							'label' => 'County',
							'type'	=> 'text',
							'class' => 'delivery-county',
							'data-stripe' => 'delivery-county',
							'value' => '',
						),
						array(
							'label' => 'Post Code',
							'type'	=> 'text',
							'class' => 'delivery-postcode',
							'data-stripe' => 'delivery-postcode',
							'value' => '',
						),

						array(
							'label' => 'Recipient Details',
							'description' => 'We need information about who you would like to give this gift to',
							'type'	=> 'section',
							'class' => 'recipient-details',
							'data-stripe' => 'recipient-details',
							'show-for' => 'gift',
						),
						array(
							'label' => 'First Name',
							'type'	=> 'text',
							'class' => 'first-name',
							'data-stripe' => 'recipient-first-name',
							'value' => '',
							'show-for' => 'gift',
						),
						array(
							'label' => 'Last Name',
							'type'	=> 'text',
							'class' => 'last-name',
							'data-stripe' => 'recipient-last-name',
							'value' => '',
							'show-for' => 'gift',
						),
						array(
							'label' => 'Street Address',
							'type'	=> 'text',
							'class' => 'recipient-street-address',
							'data-stripe' => 'recipient-street-address',
							'value' => '',
							'show-for' => 'gift',
						),
						array(
							'label' => 'Address Line 2',
							'type'	=> 'text',
							'class' => 'recipient-address-line-2',
							'data-stripe' => 'recipient-address-line-2',
							'value' => '',
							'show-for' => 'gift',
						),
						array(
							'label' => 'City',
							'type'	=> 'text',
							'class' => 'recipient-city',
							'data-stripe' => 'recipient-city',
							'value' => '',
							'show-for' => 'gift',
						),
						array(
							'label' => 'County',
							'type'	=> 'text',
							'class' => 'recipient-county',
							'data-stripe' => 'recipient-county',
							'value' => '',
							'show-for' => 'gift',
						),
						array(
							'label' => 'Post Code',
							'type'	=> 'text',
							'class' => 'recipient-postcode',
							'data-stripe' => 'recipient-postcode',
							'value' => '',
							'show-for' => 'gift',
						),
						array(
							'label' => 'Email Address',
							'type'	=> 'email',
							'class' => 'recipient-email-address',
							'data-stripe' => 'recipient-email-address',
							'value' => '',
							'show-for' => 'gift',
						),
						array(
							'label' => 'Gift Options',
							'type'	=> 'section',
							'class' => 'payment-information',
							'data-stripe' => 'payment-information',
							'show-for' => 'gift',
						),
						array(
							'label' => 'Start Month',
							'type'	=> 'select',
							'class' => 'recipient-start-month',
							'data-stripe' => 'recipient-start-month',
							'show-for' => 'gift',
							'data' => array(
								'01' => 'January ' . (( date('n') >= 1 ) ? ( date('Y') + 1 ) : date('Y')),
								'02' => 'February ' . (( date('n') >= 2 ) ? ( date('Y') + 1 ) : date('Y')),
								'03' => 'March ' . (( date('n') >= 3 ) ? ( date('Y') + 1 ) : date('Y')),
								'04' => 'April ' . (( date('n') >= 4 ) ? ( date('Y') + 1 ) : date('Y')),
								'05' => 'May ' . (( date('n') >= 5 ) ? ( date('Y') + 1 ) : date('Y')),
								'06' => 'June ' . (( date('n') >= 6 ) ? ( date('Y') + 1 ) : date('Y')),
								'07' => 'July ' . (( date('n') >= 7 ) ? ( date('Y') + 1 ) : date('Y')),
								'08' => 'August ' . (( date('n') >= 8 ) ? ( date('Y') + 1 ) : date('Y')),
								'09' => 'September ' . (( date('n') >= 9 ) ? ( date('Y') + 1 ) : date('Y')),
								'10' => 'October ' . (( date('n') >= 10  )? ( date('Y') + 1 ) : date('Y')),
								'11' => 'November ' . (( date('n') >= 11  )? ( date('Y') + 1 ) : date('Y')),
								'12' => 'December ' . (( date('n') >= 12  )? ( date('Y') + 1 ) : date('Y')),
							),
						),
						array(
							'label' => 'Message',
							'type'	=> 'textarea',
							'class' => 'recipient-message',
							'data-stripe' => 'recipient-message',
							'show-for' => 'gift',
						),
						array(
							'label' => 'Payment Information',
							'description' => 'Please enter the card information for your payment',
							'type'	=> 'section',
							'class' => 'payment-information',
							'data-stripe' => 'payment-information',
						),
						array(
							'label' => 'Card Number',
							'type'	=> 'text',
							'class' => 'str-card-number',
							'data-stripe' => 'number',
							'value' => '',
						),
						array(
							'label' => 'Expiration (MM/YY)',
							'type'	=> 'section',
							'class' => 'expiry-date',
							'data-stripe' => 'expiry-date',
						),
						array(
							'label' => 'mm',
							'type'	=> 'text',
							'class' => 'str-expiry-date',
							'data-stripe' => 'exp_month',
							'value' => '',
						),
						array(
							'label' => 'yy',
							'type'	=> 'text',
							'class' => 'str-expiry-date',
							'data-stripe' => 'exp_year',
							'value' => '',
						),
						array(
							'label' => 'Security Code (CVC)',
							'type'	=> 'text',
							'class' => 'str-security-code',
							'data-stripe' => 'cvc',
							'value' => '',
						),
						array(
							'label' => 'Discount Code',
							'type'	=> 'section',
							'class' => 'discount-code',
							'data-stripe' => 'discount-code',
              'show-for' => 'subscription',
						),
						array(
							'label' => 'Discount Code',
							'type'	=> 'text',
							'class' => 'str-discount-code',
							'data-stripe' => 'discount-code',
							'value' => '',
              'show-for' => 'subscription',
						),
						array(
							'label' => 'Please certify that you are at least 18 years of age',
							'type'	=> 'checkbox',
							'class' => 'age-verification',
							'data-stripe' => 'age-verification',
							'value' => '',
						),
						array(
							'label' => 'Confirm Order',
							'type'	=> 'submit',
							'class' => 'confirm-order',
							'data-stripe' => 'confirm-order',
							'value' => '',
						),
						array(
							'label' => '3',
							'type'	=> 'hidden',
							'class' => 'form-page-number',
							'data-stripe' => 'form-page-number',
							'value' => '',
						),
					);

					echo es_form_builder ( $args, $private = true );?>
				</ul>
			</form>
		</div>
	</div>
</section>
