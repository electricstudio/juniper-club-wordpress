<?php get_template_part( 'partials/form-page/form-header' ); ?>
<div class="subscription-form-page">
	<div class="ajax-loading-image"></div>
	<div class="ajax-container">
		<?php if ( is_user_logged_in() ) { ?>
			<input type="hidden" name="user_id" id="user_id" value="<?php echo get_current_user_id() ?>"/>

			<?php if ( array_key_exists( 'sub_method', $_GET ) ) { ?>
				<input type="hidden" name="sub_method" id="sub_method" value="<?php echo $_REQUEST['sub_method'] ?>" />
			<?php }

			if ( array_key_exists( 'sub_type', $_GET ) ) {
				$_REQUEST['subscription_type'] = $_GET['sub_type']; ?>
				<input type="hidden" name="subscription_type" id="subscription_type" value="<?php echo $_REQUEST['subscription_type'] ?>" />
				<?php get_template_part( 'partials/form-page/form-section-3' );
			} else {
				get_template_part( 'partials/form-page/form-section-2' );
			}
		} else {
			if ( array_key_exists( 'sub_method', $_GET ) ) { ?>
				<input type="hidden" name="sub_method" id="sub_method" value="<?php echo $_REQUEST['sub_method'] ?>" />
			<?php }

			if ( array_key_exists( 'sub_type', $_GET ) ) {
				$_REQUEST['subscription_type'] = $_GET['sub_type']; ?>
				<input type="hidden" name="subscription_type" id="subscription_type" value="<?php echo $_REQUEST['subscription_type'] ?>" />
			<?php }
			get_template_part( 'partials/form-page/form-section-1' );
		} ?>
	</div>
</div>