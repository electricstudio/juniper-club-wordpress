<div class="row">
	<div class="small-12 columns">
		<div class="bordered-container">
			<svg class="flourish top-left"><use xlink:href="#top-left-corner" /></svg>
			<div class="row large-collapse">
				<div class="small-12 large-10 large-centered columns text-center">
					<?php echo apply_filters( 'the_content', get_sub_field( 'box_content' ) ) ?>
				</div>
			</div>
			<svg class="flourish bottom-right"><use xlink:href="#bottom-right-corner" /></svg>
		</div>
	</div>
</div>