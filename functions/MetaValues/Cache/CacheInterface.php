<?php

namespace ES\Cache;

interface CacheInterface
{
    public function get($key, callable $resolver);

    public function put($key, $data);

    public function key($key);

    public function forget($key);
}
