<?php

namespace ES\Escaper;

class Url
{
    public function handle($value)
    {
        return esc_url($value);
    }
}
