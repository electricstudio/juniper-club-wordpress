<?php

namespace ES\Retriever;

class Group
{
    public function handle(array $value, array $data)
    {
        $response = [];

        foreach ($value as $index => $group) {
            foreach ($group as $key => $value) {
                $response[$index][str_replace('_es_', '', $key)] = $value;
            }
        }

        return $response;
    }
}
