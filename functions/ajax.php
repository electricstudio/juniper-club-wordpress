<?php

add_action( 'wp_ajax_es_ajax_form', 'es_ajax_form' );
add_action( 'wp_ajax_nopriv_es_ajax_form', 'es_ajax_form' );

function es_ajax_form() {

	$req = array_map( 'esc_attr', $_REQUEST );
	$errors = array();
	if ( array_key_exists( 'page_number', $req ) ) {
		$page_number = $req['page_number'];

		switch ( $page_number ) {
			case 1:

				if ( $req['password'] ) {
					if ( $req['password'] !== $req['confirm_password']) {
						$errors[] = 'Passwords need to match';
					}
				} else {
					$errors[] = 'Please enter a password';
				}

				if ( !strpos( $req['email_address'], '@')) {
					$errors[] = 'Please enter a valid email address';
				}

				if ( email_exists( $req['email_address'] ) ) {
					$errors[] = 'Email already in use - <a href="' . wp_login_url( the_permalink() ) . '">Login?</a>' ;
				}

				if ( $errors ) { ?>
					<section class="page-section white no-bottom-padding">
						<div class="row">
							<div class="small-12 medium-9 medium-centered large-6 columns">
								<div class="row">
									<div class="small-12 columns">
										<div class="bordered-container subscription-round-up error-form">
											<ul class="form-errors">
												<?php foreach ($errors as $error) { ?>
													<li><?php echo $error; ?></li>
												<?php } ?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<?php get_template_part( 'partials/form-page/form-section-1' );
				} else {
					$new_user_id = wp_create_user ( $req['email_address'], $req['password'], $req['email_address'] ); ?>
					<input type="hidden" name="user_id" id="user_id" value="<?php echo $new_user_id ?>"/>
					<?php if( array_key_exists( 'subscription_method', $req) ) { ?>
						<input type="hidden" name="sub_method" id="sub_method" value="<?php echo $req['subscription_method'] ?>" />
					<?php }
					if( array_key_exists( 'subscription_type', $req) ) { ?>
						<input type="hidden" name="subscription_type" id="subscription_type" value="<?php echo $req['subscription_type'] ?>" />
						<?php get_template_part( 'partials/form-page/form-section-3' );
					} else {
						get_template_part( 'partials/form-page/form-section-2' );
					}
				}

				break;

			case 2:
			 	$req = array_map( 'esc_attr', $_REQUEST ); ?>
			 	<input type="hidden" name="user_id" value="<?php echo $req['user_id'] ?>" />
			 	<input type="hidden" name="subscription_type" id="subscription_type" value="<?php echo $req['subscription_type'] ?>" />
				<?php if ( array_key_exists( 'subscription_method', $req ) ) { ?>
					<input type="hidden" name="sub_method" id="sub_method" value="<?php echo $req['subscription_method'] ?>" />
				<?php }
				get_template_part( 'partials/form-page/form-section-3' );

				break;
			case 4:
				$req = array_map( 'esc_attr', $_REQUEST );

				$subscription_type = $req[ 'subscription_type' ];

				if ( !$req['first_name'] ) {
					$errors[] = 'Please enter a first name';
				}

				if ( !$req['last_name'] ) {
					$errors[] = 'Please enter a last name';
				}

				if ( !$req['delivery_address_1'] ) {
					$errors[] = 'Please enter a valid address';
				}

				if ( !$req['delivery_city'] ) {
					$errors[] = 'Please enter a delivery city';
				}

				if ( !$req['delivery_postcode'] ) {
					$errors[] = 'Please enter a delivery postcode';
				}
				if ( $errors ) { ?>
					<section class="page-section white no-bottom-padding">
						<div class="row">
							<div class="small-12 medium-9 medium-centered large-6 columns">
								<div class="row">
									<div class="small-12 columns">
										<div class="bordered-container subscription-round-up error-form">
											<ul class="form-errors">
												<?php foreach ($errors as $error) { ?>
													<li><?php echo $error; ?></li>
												<?php } ?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				 	<input type="hidden" name="user_id" value="<?php echo $req['user_id'] ?>" />
				 	<input type="hidden" name="subscription_type" id="subscription_type" value="<?php echo $req['subscription_type'] ?>" />
					<?php get_template_part( 'partials/form-page/form-section-3' );

					break;
				}

				$customer = stripe_customer($req);

				if( array_key_exists( 'errors', $customer ) ) { ?>
					<section class="page-section white no-bottom-padding">
						<div class="row">
							<div class="small-12 medium-9 medium-centered large-6 columns">
								<div class="row">
									<div class="small-12 columns">
										<div class="bordered-container subscription-round-up error-form">
											<ul class="form-errors">
												<li><?php echo $customer['errors']; ?></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				 	<input type="hidden" name="user_id" value="<?php echo $req['user_id'] ?>" />
				 	<input type="hidden" name="subscription_type" id="subscription_type" value="<?php echo $req['subscription_type'] ?>" />
					<?php get_template_part( 'partials/form-page/form-section-3' );
					break;
				}

				$charge = false;

				$user_obj = get_userdata( $req['user_id'] );

        $mailchimp_args = array(
            'email' => $user_obj->user_email,
            'status' => 'subscribed',
        );

        $mailchimp_unsubscribe_newsletter = unsubscribe_user_mailchimp( $mailchimp_args, '84b022115b' );


				if ( $req['subscription_method'] == 'gift' ) {
					$mailchimp_subscribe_members = subscribe_user_mailchimp( $mailchimp_args, 'd13ec55c85' );
					$charge = stripe_charge( $customer, $req );

					if( array_key_exists( 'errors', $charge ) ) { ?>
						<section class="page-section white no-bottom-padding">
							<div class="row">
								<div class="small-12 medium-9 medium-centered large-6 columns">
									<div class="row">
										<div class="small-12 columns">
											<div class="bordered-container subscription-round-up error-form">
												<ul class="form-errors">
													<li><?php echo $charge['errors']['message']; ?></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					 	<input type="hidden" name="user_id" value="<?php echo $req['user_id'] ?>" />
					 	<input type="hidden" name="subscription_type" id="subscription_type" value="<?php echo $req['subscription_type'] ?>" />
						<?php get_template_part( 'partials/form-page/form-section-3' );
						break;
					}

					$mailchimp_args = array(
						'email' => $req['recipient_email'],
						'status' => 'subscribed',
					);

					$mailchimp_unsubscribe_newsletter = unsubscribe_user_mailchimp( $mailchimp_args );
					$mailchimp_subscribe_members = subscribe_user_mailchimp( $mailchimp_args, '8fe8a3a1f5' );
				}

				$new_post_args = array(
					'post_type' => 'subscriptions',
					'post_author' => $req['user_id'],
					'post_title' => $customer->id,
					'post_status' => 'publish',
				);

				$post_id = wp_insert_post( $new_post_args );

				$user_info = array(
					'first_name' => $req['first_name'],
					'last_name' => $req['last_name'],
					'contact_phone' => $req['contact_phone'],
					'delivery_address_1' => $req['delivery_address_1'],
					'delivery_address_2' => $req['delivery_address_2'],
					'delivery_city' => $req['delivery_city'],
					'delivery_county' => $req['delivery_county'],
					'delivery_postcode' => $req['delivery_postcode'],
					'customer_id' => $customer->id,
					'subscription_method' => $req['subscription_method'],
					'subscription_type' => $req['subscription_type'],
					'subscription_date' => date( 'd-m-Y', get_next_cutoff() ),
					'recipient_email' => $user_obj->user_email,
					'discount_code' => $req['discount_code']
				);

				if ( $req['subscription_method'] == 'gift' ) {
					$user_info['recipient_first_name'] = $req['recipient_first_name'];
					$user_info['recipient_last_name'] = $req['recipient_last_name'];
					$user_info['recipient_address_1'] = $req['recipient_address_1'];
					$user_info['recipient_address_2'] = $req['recipient_address_2'];
					$user_info['recipient_city'] = $req['recipient_city'];
					$user_info['recipient_county'] = $req['recipient_county'];
					$user_info['recipient_postcode'] = $req['recipient_postcode'];
					$user_info['recipient_email'] = $req['recipient_email'];
					$user_info['recipient_message'] = $req['recipient_message'];

					$dates['sub_date'] = strtotime( '05-' . $req['recipient_start_month'] .'-' . date( 'Y' ) );
					$dates['today'] = time();

					$date_sum = $dates['sub_date'] - $dates['today'];

					if ( $date_sum <= 0 ) {
						$dates['sub_date'] = strtotime( '05-' . $req['recipient_start_month'] . '-' . date( 'Y', strtotime( date( "d-m-Y", mktime() ) . " + 365 day" ) ) );
					}

					$user_info['subscription_date'] = date( 'd-m-Y', $dates['sub_date'] );
				}

				if ( $charge ) {
					$user_info['charge'] = $charge->id;
				}

				foreach ( $user_info as $key => $user_meta ) {
					update_field( 'sub_' . $key, $user_meta, $post_id );
				}

				if ( $req['subscription_method'] == 'subscription' ) {

					$mailchimp_subscribe_members = subscribe_user_mailchimp( $mailchimp_args, '84b022115b' );

					send_customer_email( $post_id ); ?>
					<section class="page-section white form-page">
						<input type="hidden" name="form-page-number" id="form-page-number" value="confirmation">
						<div class="row margin-bottom">
							<div class="small-12 columns text-center">
								<h1>Welcome to the Juniper Club</h1>
								<a href="<?php echo get_permalink( $post_id ); ?>" class="view-subscription">Click here to view your subscription</a>
							</div>
						</div>
					</section>
				<?php } else {
					send_purchaser_email( $post_id ); ?>
					<section class="page-section white form-page">
						<input type="hidden" name="form-page-number" id="form-page-number" value="confirmation">
						<div class="row margin-bottom">
							<div class="small-12 columns text-center">
								<h1>Thank you for your purchase.</h1>
								<a href="<?php echo get_permalink( $post_id ); ?>" class="button">Click here to view your gift purchase</a>
							</div>
						</div>
					</section>
					<!-- Google Code for Juniper Club Subscription purchase Conversion Page -->
					<script type="text/javascript">
						/* <![CDATA[ */
						var google_conversion_id = 870427252;
						var google_conversion_language = "en";
						var google_conversion_format = "3";
						var google_conversion_color = "ffffff";
						var google_conversion_label = "TWMRCO3SrG4Q9NSGnwM";
						var google_remarketing_only = false;
						/* ]]> */
					</script>
					<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
					</script>
					<noscript>
					<div style="display:inline;">
					<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/870427252/?label=TWMRCO3SrG4Q9NSGnwM&amp;guid=ON&amp;script=0"/>
					</div>
					</noscript>
				<?php }

				break;
		}
	}

	wp_die();
}
