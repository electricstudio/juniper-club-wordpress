<section class="page-section patterned">
	<div class="row">
		<div class="small-12 text-center">
			<h1>Join the Juniper Club</h1>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-10 medium-centered large-8 columns">
			<div class="form-header">
				<div class="step-button step-1 toggled">
					<span class="icon">1</span>
					<p>Sign Up</p>
				</div>
				<div class="progress-bar left"></div>
				<div class="step-button step-2">
					<span class="icon">2</span>
					<p>Select Subscription</p>
				</div>
				<div class="progress-bar right"></div>
				<div class="step-button step-3">
					<span class="icon">3</span>
					<p>Checkout</p>
				</div>
			</div>
		</div>
	</div>
</section>