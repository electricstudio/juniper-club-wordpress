<?php

namespace ES\Retriever;

class Int
{
    public function handle($value)
    {
        return intval($value);
    }
}
