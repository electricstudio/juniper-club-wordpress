<div class="row blog-post">
	<div class="small-12 columns">
		<div class="bordered-container">
			<svg class="flourish top-left"><use xlink:href="#top-left-corner" /></svg>
			<div class="row large-collapse">
				<div class="small-12 large-10 large-centered columns">
					<div class="row" data-equalizer>
						<div class="small-12 large-5 columns">
							<div class="post-thumb" style="background: url('<?php echo the_post_thumbnail_url( 'latest-post-thumb' ); ?>') center no-repeat; background-size: cover;" data-equalizer-watch></div>
						</div>
						<div class="small-12 large-6 large-offset-1 columns" data-equalizer-watch>
							<p class="post-date"><?php the_time('jS, F Y') ?></p>
							<?php the_title( '<h3>', '</h3>' ) ?>
							<?php the_excerpt(); ?>
							<a class="button" href="<?php the_permalink() ?>">Read More</a>
						</div>
					</div>
				</div>
			</div>
			<svg class="flourish bottom-right"><use xlink:href="#bottom-right-corner" /></svg>
		</div>
	</div>
</div>