# Electric Studio Boilerplate

Boilerplate theme based on top of Foundation 5, Bower and Gulp.



## Requirements

You'll need to have the following items installed before continuing.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Gulp](http://gulpjs.com/): Run `[sudo] npm install -g gulp`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`
  * [scss-lint](https://github.com/brigade/scss-lint): Run `[sudo] gem install scss_lint`
  * [WordPress Coding Standards](https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards)

### Installing WordPress Coding Standards
1. Install [homebrew](http://brew.sh/) `ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
2. install composer
```bash
brew update
brew tap homebrew/dupes
brew tap homebrew/php
brew install composer
```
3. add composer to PATH
in ~/.bash_profile: add the line : `export PATH=~/.composer/vendor/bin:$PATH`
4. install phpcs
`composer global require "squizlabs/php_codesniffer=*"`
5. install wpcs (run this in the root of your Sites folder)
`composer create-project wp-coding-standards/wpcs:dev-master --no-dev`
6. Add its path to PHP_CodeSniffer configuration:
`phpcs --config-set installed_paths /path/to/wpcs`
7. test it works in this folder of this repository:
`phpcs -s -v -p --standard=./code.ruleset.xml ./**/*.php *.php`


## Linting

Built in to this boilerplate comes full js, scss and WordPress linting. Scss and JS linting are part of the default gulp task but if you want to check your WordPress code is up to scratch, run `gulp lint` (this also lints the scss and js) or `gulp wordpress-lint`.




## Quickstart

```bash
git clone git@bitbucket.org:electricstudio/boilerplate.git
npm install && bower install
```
While you're working on your project, run:
`gulp`

And you're set!

