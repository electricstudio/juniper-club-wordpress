<?php
/**
 * admin.php
 *
 * This file contains all the style changes to the wp-admin interface
 *
 * @package boilerplate
 */

/**
 *
 * Adds ES logo and link to admin bar
 * @param 	object $wp_admin_bar
 * @author	Swifty <james@electricstudio.co.uk>
 * @since 	1.0
 */
function es_add_es_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
	$args = array(
		'id' => 'es_logo',
		'title' => '<span class="ab-icon"><img src="'.get_bloginfo( 'template_directory' ) . '/functions/images/es_icon.png"/></span>',
		'href' => 'http://www.electricstudio.co.uk',
	);
	$wp_admin_bar->add_node( $args );
}

/**
 *
 * Removes unnecessary widgets from the dashboard
 * @author Swifty <james@electricstudio.co.uk>
 * @since	1.0
 */
function es_remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'] );
}

// Add actions and filters for the functions in this file
add_action( 'wp_dashboard_setup', 'es_remove_dashboard_widgets' );
add_action( 'admin_bar_menu', 'es_add_es_logo', 1 );

function es_replace_logo() {
	$url = TEMPLATEURI . 'functions/images/es_logo.png';
	?>
	<style>
		.login {
			background-image: url( <?php echo TEMPLATEURI . 'images/patterned-section-bg.jpg' ?> ); ?>);
		}
		.login h1 a {
			background-image: url(<?php echo esc_url( $url ); ?>);
			background-size: cover;
			width: 262px;
			height: 96px;
		}
	</style>
	<?php
}
add_action( 'login_head', 'es_replace_logo' );

// only display custom menu bar if the user is logged in and is an admin
if ( current_user_can( 'update_core' ) && is_user_logged_in() ) {
	function es_admin_css() {
		?>
		<style>
			#wpadminbar {
				background-color: <?php echo esc_attr( HEADERCOLOR ); ?>;
				will-change: transform;
			}
		</style>
		<?php
	}
	add_action( 'wp_head', 'es_admin_css' );
	add_action( 'admin_head', 'es_admin_css' );
}
