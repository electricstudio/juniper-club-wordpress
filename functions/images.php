<?php
add_theme_support( 'post-thumbnails' );

/**
 * Add new image sizes
 * Structure is 'name' => array( $width, $height, $crop )
 */

global $picture_sizes;
$picture_sizes = array(
	'small-img' => array( 300, 200, true ),
	'medium-img' => array( 700, 372, false ),
	'large-img' => array( 1000, 702, true ),
	'featured-img' => array( 1600, 632, true  ),
	'testimonial-img' => array( 128, 128, true ),
	'post-thumb' => array( 800, 632, true ),
);

foreach ( $picture_sizes as $name => $data ) {
	add_image_size( $name, $data[0], $data[1], $data[2] );
}

/**
 * Adds the medium and the full to the image size list in the editor, so people can only insert them
 * into pages and articles.
 */
function es_add_additional_image_sizes( $sizes ) {
	global $_wp_additional_image_sizes;

	if ( empty( $_wp_additional_image_sizes ) ) {
		return $sizes;
	}

	foreach ( $_wp_additional_image_sizes as $id => $data ) {
		if ( ! isset( $sizes[ $id ] ) ) {
			$sizes[ $id ] = ucfirst( str_replace( '-', ' ', $id ) );
		}
	}

	return $sizes;
}
add_filter( 'image_size_names_choose', 'es_add_additional_image_sizes' );
