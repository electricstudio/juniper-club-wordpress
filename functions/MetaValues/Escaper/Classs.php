<?php

namespace ES\Escaper;

class Classs extends EscaperAbstract
{
    public function handle($value)
    {
        if (is_array($value)) {
            $response = esc_attr(implode(' ', $value));
        } else {
            $response = esc_attr($value);
        }

        return $response;
    }
}
