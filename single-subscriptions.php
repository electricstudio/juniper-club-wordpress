<?php get_header();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>
		<section class="page-section patterned">
			<div class="row">
				<div class="small-12 columns">
					<?php if( get_field( 'sub_subscription_method' ) == 'gift' ) {
						$cancel_page = get_page_by_path( 'cancel-subscription' ); ?>
						<h1>Your Gift Details</h1>
					<?php } else { ?>
						<h1>Your Subscription Details</h1>
					<?php } ?>
				</div>
			</div>
		</section>
		<section class="page-section white">
			<?php get_template_part( 'partials/active-subscription' ); ?>
		</section>
		<section class="page-section white">
			<div class="row">
				<div class="small-12 medium-6 medium-centered text-center">
					<?php $account_page = get_page_by_path( 'my-account' ); ?>
					<a href="<?php echo get_permalink( $account_page->ID ) ?>" class="button">Back to My Account</a>
					<?php $update_page = get_page_by_path( 'update-details' ); ?>
					<a href="<?php echo get_permalink( $update_page->ID ) ?>?cust_id=<?php echo get_field( 'sub_customer_id' ) ?>" class="button">Update Details</a>
					<?php if( get_field( 'sub_subscription_method' ) !== 'gift' ) {
						$cancel_page = get_page_by_path( 'cancel-subscription' ); ?>
						<a href="<?php echo get_permalink( $cancel_page->ID ) ?>?cust_id=<?php echo get_field( 'sub_customer_id' ) ?>" class="button unstyled">Cancel</a>
					<?php } ?>
				</div>
			</div>
		</section>
	<?php }
}
get_footer();

