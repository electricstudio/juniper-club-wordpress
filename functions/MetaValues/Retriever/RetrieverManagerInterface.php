<?php

namespace ES\Retriever;

interface RetrieverManagerInterface
{
    public function manage(array $meta_keys, $pid, $prefix);
}
