<?php

namespace ES\Escaper;

interface EscaperManagerInterface
{
    public function manage($method, $value);
}
