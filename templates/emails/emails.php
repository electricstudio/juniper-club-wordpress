<?php

if( array_key_exists( 'send_email', $_GET ) ) {
	send_customer_email( $_GET['send_email'] );
}

function send_customer_email ( $customer_post_id ) {

	$customer_post = get_post( $customer_post_id );
	$customer_post_data = get_post_custom( $customer_post_id );

	$customer_user = get_userdata( $customer_post->post_author );
	$customer_email = $customer_user->user_email;

	$subject = 'The Juniper Club New ' . ucfirst( $customer_post_data['sub_subscription_type'][0] ) . ' subscription';

	$headers = array(
		'Content-Type: text/html; charset=UTF-8',
		'From: The Juniper Club <info@juniperclub.co.uk>',
		'CC: info@juniperclub.co.uk'
	);

	$body = '<p>Dear ' . $customer_post_data['sub_first_name'][0] . ',</p>';
	$body .= '<p>Welcome to The Juniper Club, it’s great to have you on board!</p>';
	$body .= '<p>You have taken the exciting step to receive regular deliveries of craft gin delivered directly to your door, your ' . $customer_post_data['sub_subscription_type'][0] . ' means that your first Gin delivery will arrive with you in the middle of ' . date('F', strtotime( str_replace('/', '-', $customer_post_data['sub_subscription_date'][0] ) ) ) . ' following the charge to your payment card on the 5th of ' . date('F', strtotime( str_replace('/', '-', $customer_post_data['sub_subscription_date'][0] ) ) ) . '</p>';
	$body .= '<p>If you wish to change any details of your subscription this is all accessible via your account which was created when you subscribed, we hope you don’t but you can also cancel at any time.</p>';
	$body .= '<p>If you are a social media creature it would be great to connect with you online, please say hello soon on Twitter and Facebook.</p>';
	$body .= '<p>If you have any questions we are here for you, please email us via <a href="mailto:info@juniperclub.co.uk">info@juniperclub.co.uk</a> and we will get back to you as soon as possible.</p>';

	$body .= '<p>Best Wishes</p>';
	$body .= '<p>The Juniper Club Team,<br>‘It’s a Gin thing!’</p>';

	$email_sent = wp_mail( $customer_email, $subject, $body, $headers );
}

function send_purchaser_email ( $customer_post_id ) {

	$customer_post = get_post( $customer_post_id );
	$customer_post_data = get_post_custom( $customer_post_id );

	$customer_user = get_userdata( $customer_post->post_author );
	$customer_email = $customer_user->user_email;

	$subject = 'The Juniper Club ' . ucfirst( $customer_post_data['sub_subscription_type'][0] ) . ' gift purchase confirmed';

	$headers = array(
		'Content-Type: text/html; charset=UTF-8',
		'From: The Juniper Club <info@juniperclub.co.uk>',
		'CC: info@juniperclub.co.uk'
	);

	switch ( $customer_post_data['sub_subscription_type'][0] ) {
		case 'monthly':
			$gin_deliveries = 12;
			break;

		case 'bi-monthly':
			$gin_deliveries = 6;
			break;

		case 'quarterly':
			$gin_deliveries = 4;
			break;
	}



	$body = '<p>Dear ' . $customer_post_data['sub_first_name'][0] . ',</p>';

	$body .= '<p>Your Juniper Club Gift purchase for ' . $customer_post_data['sub_recipient_first_name'][0] . ' ' . $customer_post_data['sub_recipient_last_name'][0] . ' has been received and they will receive their first delivery in mid ' . date('F', strtotime( str_replace('/', '-', $customer_post_data['sub_subscription_date'][0] ) ) ) . '.</p>';
	$body .= '<p>Their gift type means that ' . $customer_post_data['sub_recipient_first_name'][0] . ' will receive ' . $gin_deliveries . ' deliveries of craft Gin in the next 12 months.</p>';
	$body .= '<p>If you are a social media creature it would be great to connect with you online, please say hello soon on Twitter and Facebook.</p>';
	$body .= '<p>If you have any questions we are here for you, please email us via <a href="mailto:info@juniperclub.co.uk">info@juniperclub.co.uk</a> and we will get back to you as soon as possible.</p>';

	$body .= '<p>Best Wishes</p>';
	$body .= '<p>The Juniper Club Team,<br>‘It’s a Gin thing!’</p>';

	$email_sent = wp_mail( $customer_email, $subject, $body, $headers );
}

function send_cancelled_email ( $customer_post_id ) {

	$customer_post = get_post( $customer_post_id );
	$customer_post_data = get_post_custom( $customer_post_id );

	$customer_user = get_userdata( $customer_post->post_author );
	$customer_email = $customer_user->user_email;

	$subject = 'Juniper club subscription cancelled';

	$headers = array(
		'Content-Type: text/html; charset=UTF-8',
		'From: The Juniper Club <info@juniperclub.co.uk>',
		'CC: info@juniperclub.co.uk'
	);

	$body = '<p>Dear ' . $customer_post_data['sub_first_name'][0] . ',</p>';

	$body .= '<p>This is an email confirmation that we have received a request in your account to cancel your regular subscription to The Juniper Club.</p>';
	$body .= '<p>We are sorry to see you go and hope you have enjoyed sampling the craft gins sent to you with your subscription.</p>';
	$body .= '<p>We would be keen to receive any feedback from you so if there are any comments you would like to make about the website or service please do let us know.</p>';

	$body .= '<p>Best Wishes</p>';
	$body .= '<p>The Juniper Club Team,<br>‘It’s a Gin thing!’</p>';

	$email_sent = wp_mail( $customer_email, $subject, $body, $headers );
}