<?php

/**Disable admin bar if not admin */
add_action( 'after_setup_theme', 'remove_admin_bar' );

function remove_admin_bar() {
	if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
		show_admin_bar( false );
	}
}

/* redirect users to front page after login */
function redirect_to_front_page() {
	global $redirect_to;
	if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
		if ( ! isset( $_GET['redirect_to'] ) ) {
			$redirect_to = get_option( 'siteurl' ) . '/my-account';
		}
	}
}
add_action( 'login_form', 'redirect_to_front_page' );

/**
 * Gives the div a class name reflective
 * of the file name.
 * e.g.row-content.php becomes
 * row-content
 */
if ( ! function_exists( 'es_section_class' ) ) {
	function es_section_class( $file ) {
		echo esc_attr( basename( preg_replace( '/\.php$/', '', $file ) ) );
	}
}

/**
 * Get template wrapper for partials
 */
if ( ! function_exists( 'es_partial' ) ) {
	function es_partial( $basename, $directory = 'partials/row' ) {
		return get_template_part( $directory, $basename );
	}
}

/**
 * pass global $post object
 */
if ( ! function_exists( 'es_page_is_parent' ) ) {
	function es_page_is_parent( $post ) {
		return is_page() && 0 === intval( $post->post_parent );
	}
}

/**
 * prefix templates with template-{name}.php
 */
if ( ! function_exists( 'es_is_template' ) ) {
	function es_is_template( $basename ) {
		return is_page_template( 'templates/template-' . $basename . '.php' );
	}
}

/**
 * Limit word output
 */
if ( ! function_exists( 'es_limit_words' ) ) {

	function es_limit_words( $string, $word_limit, $end = '' ) {

		$words = explode( ' ', $string, ( $word_limit + 1 ) );

		if ( count( $words ) > $word_limit ) {
			array_pop( $words );
			//add a . at last article when more than limit word count
			return rtrim( implode( ' ', $words ), ',' ) . $end;
		} else {
			return rtrim( implode( ' ', $words ), ',' );
		}
	}
}

if ( ! function_exists( 'es_preit' ) ) {
	function es_preit( $obj, $echo = true ) {
		if ( $echo ) {
			echo '<pre>' . esc_html( print_r( $obj, true ) ) . '</pre>';
		} else {
			return '<pre>' . esc_html( print_r( $obj, true ) ) . '</pre>';
		}
	}
}

if ( ! function_exists( 'es_silent' ) ) {
	function es_silent( $obj ) {
		echo '<pre style="display: none;">' . esc_html( print_r( $obj, true ) ) . '</pre>';
	}
}

/**
 * To avoid php notices for missing indexes
 * @param  string  			$key     		the key of the meta
 * @param  array 			$custom  		the entire custom object with get_post_custom
 * @param  boolean 			$all     		whether to include ALL, or just one singular
 * @param  string  			$default 		what the value should be returned in case there's no index like that
 * @return mixed           					whatever the custom holds
 */
if ( ! function_exists( 'es_custom' ) ) {
	function es_custom( $key, $custom, $all = false, $default = '' ) {
		return is_array( $custom ) ? ( array_key_exists( $key, $custom ) ) ? ( $all ) ? $custom[ $key ] : $custom[ $key ][0] : $default : $default;
	}
}

/*
 * Define Constants for use in theme
 */
if ( ! function_exists( 'es_init_constants' ) ) {
	function es_init_constants() {
		$theme_mods = get_theme_mods();

		if ( ! defined( 'TEMPLATEURI' ) ) {
			define( 'TEMPLATEURI', trailingslashit( get_stylesheet_directory_uri() ) );
		}

		if ( ! defined( 'HOMEURL' ) ) {
			define( 'HOMEURL', trailingslashit( get_home_url() ) );
		}

		if ( ! defined( 'THEMECOLOR' ) ) {
			if ( ! empty( $theme_mods['es_theme_color'] ) ) {
				$meta_color = $theme_mods['es_theme_color'];;
			} else {
				$meta_color = '#000000';
			}

			define( 'THEMECOLOR', $meta_color );
		}

		if ( ! defined( 'HEADERCOLOR' ) ) {
			if ( ! empty( $theme_mods['es_header_color'] ) ) {
				$meta_color = $theme_mods['es_header_color'];
			} else {
				$meta_color = '#000000';
			}

			define( 'HEADERCOLOR', $meta_color );
		}
	}
	add_action( 'init', 'es_init_constants' );
}


// Set theme-color meta value
if ( ! function_exists( 'es_header_theme_color' ) ) {
	function es_header_theme_color() {
		echo '<meta name="theme-color" content="' . esc_attr( THEMECOLOR ) . '">';
	}
	add_action( 'wp_head', 'es_header_theme_color' );
}


// This fixes the broken floating wordpress admin menu that you get when window < 600px
if ( ! function_exists( 'es_admin_mobile_menu_fix' ) && is_user_logged_in() ) {
	function es_admin_mobile_menu_fix() {
		echo '<style>@media (max-width: 600px) { #wpadminbar { position: fixed; } }</style>';
	}
	add_action( 'wp_head', 'es_admin_mobile_menu_fix' );
}

/* Removes emoji script and styling that was added in release of WordPress 4.2 */
if ( ! function_exists( 'es_remove_emojis' ) ) {
	function es_remove_emojis() {
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	}
	add_action( 'init', 'es_remove_emojis' );
}

/*
 * Adds the title tag due to wp_title() being deprecated from WordPress version 4.4
 */

if ( ! function_exists( 'es_add_title_tag' ) ) {
	function es_add_title_tag() {
		add_theme_support( 'title-tag' );
	}
	add_action( 'after_setup_theme', 'es_add_title_tag' );
}

if ( ! function_exists( 'es_remove_wp_version' ) ) {
	function es_remove_wp_version() {
		remove_action( 'wp_head', 'wp_generator' );
	}
	add_action( 'init', 'es_remove_wp_version' );
}

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page();
}

function es_form_builder( $args, $private = false ) {
	$html = '';
	foreach ( $args as $input ) {
		if ( ! array_key_exists( 'show-for', $input ) ) {
			$input['show-for'] = 'both';
		}

		if ( ! array_key_exists( 'value', $input ) ) {
			$input['value'] = false;
		}

		$html .= '<li class="input-type-' . esc_html( $input['type'] ) .' show-for-' . $input['show-for'] . ' class-' . $input['class'] . '">';
		switch ( $input['type'] ) {
			case 'checkbox' :
				if ( $private ) {
					$html .= '<input type="' . esc_html( $input['type'] ) . '" data-stripe="' . esc_html( $input['data-stripe'] ) . '"></input>';
					$html .= '<label for="' . esc_html( $input['class'] ) . '">' . esc_html( $input['label'] ) . '</label>';
					break;
				}
				$html .= '<input type="' . esc_html( $input['type'] ) . '" name="' . esc_html( $input['class'] ) . '" id="' . esc_html( $input['class'] ) . '" data-stripe="' . esc_html( $input['data-stripe'] ) . '"></input>';
				$html .= '<label for="' . esc_html( $input['class'] ) . '">' . esc_html( $input['label'] ) . '</label>';
				break;

			case 'section' :
				$html .= '<h4><strong>' . $input['label'] . '</strong></h4>';

				if ( array_key_exists( 'description', $input ) ) {
					$html .= '<p>' . $input['description'] . '</p>';
				}

				break;

			case 'select' :
				$html .= '<label for="' . esc_html( $input['class'] ) . '">' . esc_html( $input['label'] ) . '</label>';
				$html .= '<select name="' . esc_html( $input['class'] ) . '" id="' . esc_html( $input['class'] ) . '" data-stripe="' . esc_html( $input['data-stripe'] ) . '">';
				foreach ( $input['data'] as $key => $value ) {
					$html .= '<option value="' . $key .'">' . $value . '</option>';
				}
				$html .= '</select>';
				break;

			case 'textarea' :
				$html .= '<label for="' . esc_html( $input['class'] ) . '">' . esc_html( $input['label'] ) . '</label>';
				$html .= '<textarea name="' . esc_html( $input['class'] ) . '" id="' . esc_html( $input['class'] ) . '" data-stripe="' . esc_html( $input['data-stripe'] ) . '"></textarea>';
				break;

			case 'submit' :
				$html .= '<button class="button" type="' . esc_html( $input['type'] ) . '" name="' . esc_html( $input['class'] ) . '" id="' . esc_html( $input['class'] ) . '" value="' . esc_html( $input['class'] ) . '">' . esc_html( $input['label'] ) . '</button>';
				break;

			case 'hidden' :
				$html .= '<input type="' . esc_html( $input['type'] ) . '" name="' . esc_html( $input['class'] ) . '" id="' . esc_html( $input['class'] ) . '" value="' . esc_html( $input['label'] ) . '"></input>';
				break;

			default:
				if ( $private ) {
					$html .= '<label for="' . esc_html( $input['class'] ) . '">' . esc_html( $input['label'] ) . '</label>';
					$html .= '<input type="' . esc_html( $input['type'] ) . '" data-stripe="' . esc_html( $input['data-stripe'] ) . '" value="' . esc_html( $input['value'] ) . '"></input>';
					break;
				}
				$html .= '<label for="' . esc_html( $input['class'] ) . '">' . esc_html( $input['label'] ) . '</label>';
				$html .= '<input type="' . esc_html( $input['type'] ) . '" name="' . esc_html( $input['class'] ) . '" id="' . esc_html( $input['class'] ) . '" data-stripe="' . esc_html( $input['data-stripe'] ) . '" value="' . esc_html( $input['value'] ) . '"></input>';
				break;
		}
		$html .= '</li>';
	}
	return $html;
}

add_action( 'pre_get_posts', 'archives_changes' );

function archives_changes( $query ) {
	if ( ! is_admin() && is_post_type_archive( 'gins' ) && $query->is_main_query() ) {
		$query->set( 'offset', 1 );
		$query->set( 'posts_per_page', -1 );
	}

	return $query;
}

/*
* Callback function to filter the MCE settings
*/

function wpb_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter( 'mce_buttons_2', 'wpb_mce_buttons_2' );

function my_mce_before_init_insert_formats( $init_array ) {

	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'Secondary Colour',
			'block' => 'span',
			'classes' => 'secondary-color',
			'wrapper' => true,

		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;
}

add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

function get_next_cutoff() {
	$today = date( 'j' );

	if ( $today > 5 ) {
		$new_cutoff = date( 'n' ) + 1;

		if ( $new_cutoff == 13 ) {
			$cutoff['day'] = 5;
			$cutoff['month'] = 01;
			$cutoff['year'] = date( 'Y' ) + 1;

			return strtotime( $cutoff['month'] . '/' . $cutoff['day'] . '/' . $cutoff['year'] );
		}

		$cutoff['day'] = 5;
		$cutoff['month'] = $new_cutoff;
		$cutoff['year'] = date( 'Y' );

		return strtotime( $cutoff['month'] . '/' . $cutoff['day'] . '/' . $cutoff['year'] );
	}

	$cutoff['day'] = 5;
	$cutoff['month'] = date( 'm' );
	$cutoff['year'] = date( 'Y' );

	return strtotime( $cutoff['month'] . '/' . $cutoff['day'] . '/' . $cutoff['year'] );
}

function get_subscription_by_cust_id( $customer_id ) {
	$args = array(
		'post_type' => 'subscriptions',
		'posts_per_page' => 1,
		'meta_query' => array(
			array(
				'key' => 'sub_customer_id',
				'value' => $customer_id,
				'compare' => '='
			)
		)
	);

	$subscription_query = new WP_Query( $args );

	if ( $subscription_query->have_posts() ) {
		return $subscription_query->posts;
	}

	return false;
}


add_filter('manage_subscriptions_posts_columns', 'subscriptions_columns');
add_action('manage_subscriptions_posts_custom_column', 'subscriptions_columns_content', 10, 2);

// ADD NEW COLUMN
function subscriptions_columns( $defaults ) {
	$defaults['title'] = 'Customer ID';
	$defaults['sub_name'] = 'Name';
	$defaults['sub_method'] = 'Type';
	$defaults['sub_address_1'] = 'Address Line 1';
	$defaults['sub_address_2'] = 'Address Line 2';
	$defaults['sub_address_3'] = 'City';
	$defaults['sub_address_4'] = 'County';
	$defaults['sub_address_5'] = 'Postcode';
	$defaults['sub_type'] = 'Period';
	return $defaults;
}

function subscriptions_columns_content( $column_name, $post_id ) {
	if ($column_name == 'sub_name') {
		$name = ucfirst( get_field('sub_first_name', $post_id ) ) . ' ' . ucfirst( get_field( 'sub_last_name', $post_id ) );
	}

	if ($column_name == 'sub_address_1') {
		$name = ucfirst( get_field('sub_delivery_address_1', $post_id ) );
	}

	if ($column_name == 'sub_address_2') {
		$name = ucfirst( get_field('sub_delivery_address_2', $post_id ) );
	}

	if ($column_name == 'sub_address_3') {
		$name = ucfirst( get_field('sub_delivery_city', $post_id ) );
	}

	if ($column_name == 'sub_address_4') {
		$name = ucfirst( get_field('sub_delivery_county', $post_id ) );
	}

	if ($column_name == 'sub_address_5') {
		$name = ucfirst( get_field('sub_delivery_postcode', $post_id ) );
	}

	if ($column_name == 'sub_method') {
		$name = ucfirst( get_field('sub_subscription_method', $post_id ) );
	}

	if ($column_name == 'sub_type') {
		$name = ucfirst( get_field('sub_subscription_type', $post_id ) );
	}

	echo $name;
}
