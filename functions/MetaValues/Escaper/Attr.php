<?php

namespace ES\Escaper;

class Attr extends EscaperAbstract
{
    public function handle($value)
    {
        return esc_attr($value);
    }
}
