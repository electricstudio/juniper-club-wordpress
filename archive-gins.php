<?php get_header(); ?>
<section class="page-section patterned">
	<div class="row">
		<div class="small-12 text-center">
			<h1>The Gins</h1>
		</div>
	</div>
</section>
<section class="page-section white">
	<?php get_template_part( 'partials/latest-gin' ); ?>
</section>
<?php if ( have_posts() ) { ?>
	<section class="page-section white">
		<div class="row small-up-1 medium-up-2 large-up-3">
			<?php while ( have_posts() ) {
				the_post();
				get_template_part( 'partials/single-gin' );
			} ?>
		</div>
	</section>
<?php } ?>
<section class="page-section patterned">
	<?php get_template_part( 'partials/subscriptions-box' ); ?>
</section>
<?php get_footer();

