<?php

namespace ES\Retriever;

class SetList
{
    public function handle($value, $data)
    {
        $html = '<ul>';

        if (array_key_exists(2, $data)) {
            $html = '<ul class="'.esc_attr($data[2]).'">';
        }

        foreach ($value as $node) {
            $html .= '<li>';
            $html .= $node;
            $html .= '</li>';
        }

        $html .= '</ul>';

        return $html;
    }
}
