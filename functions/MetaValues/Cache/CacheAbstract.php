<?php

namespace ES\Cache;

abstract class CacheAbstract
{
    private $expiration;

    public function __construct($expiration)
    {
        $this->expiration = $expiration;
    }

    public function keyForMeta($meta_keys, $post_id = null)
    {
        return $this->key(serialize($meta_keys) . '_' . $post_id);
    }

    public function keyForQuery($file, $post_id, $key)
    {
        return $this->key(partial_name($file) .'_'. $post_id . '_'. $key);
    }
}
