<?php
/*
 * File name - cpts.php
 * Add a custom post type to the standard wordpress build.
 */
function es_cpts_init(){
	$gin_labels = array(
		'name' 					=> _x( 'Gins', 'post type general name' ),
		'singular_name' 		=> _x( 'Gin', 'post type singular name' ),
		'add_new' 				=> _x( 'Add New', 'Gin' ),
		'add_new_item' 			=> __( 'Add New ' . 'Gin' ),
		'edit_item' 			=> __( 'Edit ' . 'Gin' ),
		'new_item' 				=> __( 'New ' . 'Gin' ),
		'view_item' 			=> __( 'View ' . 'Gin' ),
		'search_items' 			=> __( 'Search ' . 'Gins' ),
		'not_found' 			=> __( 'No ' . 'Gins' . ' found' ),
		'not_found_in_trash' 	=> __( 'No ' . 'Gins' . ' found in Trash' ),
		'parent_item_colon' 	=> '',
	);
	// You can rewrite the slug on the front end by adding this to the key => Value on line 42 below.

	$gin_args = array(
		'labels' 				=> $gin_labels,
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'query_var' 			=> false,
		'rewrite' 				=> true, // You can use $rewrite VAR above here.
		'capability_type' 		=> 'post',
		'has_archive' 			=> true,
		'hierarchical' 			=> false,
		'menu_position' 		=> null,
		'menu_icon' 			=> 'dashicons-star-empty',
		'supports' 				=> array(
			'title',
			'thumbnail',
			'excerpt',
			'editor',
			'page-attributes',
		),
		'map_meta_cap' 			=> true,

	);

	register_post_type( 'gins', $gin_args );

	$subscription_labels = array(
		'name' 					=> _x( 'Subscriptions', 'post type general name' ),
		'singular_name' 		=> _x( 'Subscription', 'post type singular name' ),
		'add_new' 				=> _x( 'Add New', 'Subscription' ),
		'add_new_item' 			=> __( 'Add New ' . 'Subscription' ),
		'edit_item' 			=> __( 'Edit ' . 'Subscription' ),
		'new_item' 				=> __( 'New ' . 'Subscription' ),
		'view_item' 			=> __( 'View ' . 'Subscription' ),
		'search_items' 			=> __( 'Search ' . 'Subscriptions' ),
		'not_found' 			=> __( 'No ' . 'Subscriptions' . ' found' ),
		'not_found_in_trash' 	=> __( 'No ' . 'Subscriptions' . ' found in Trash' ),
		'parent_item_colon' 	=> '',
	);
	// You can rewrite the slug on the front end by adding this to the key => Value on line 42 below.

	$subscription_args = array(
		'labels' 				=> $subscription_labels,
		'public' 				=> false,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'query_var' 			=> false,
		'rewrite' 				=> true, // You can use $rewrite VAR above here.
		'capability_type' 		=> 'post',
		'has_archive' 			=> false,
		'hierarchical' 			=> false,
		'menu_position' 		=> null,
		'menu_icon' 			=> 'dashicons-calendar-alt',
		'supports' 				=> array(
			'title',
			'page-attributes',
			'author',
		),
		'map_meta_cap' 			=> true,

	);

	register_post_type( 'subscriptions', $subscription_args );
}

/*
 * Initiate the custom post type.
 */
add_action( 'init', 'es_cpts_init' );
