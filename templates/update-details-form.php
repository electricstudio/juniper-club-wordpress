<?php $show_card_error = false;

if( $_POST ) {
	$req = $_POST;

	if( array_key_exists( 'update-card-details', $_POST ) ) {
		$args = array(
			'card' => array(
				'number' => $req['str-card-number'],
				'exp_month' => $req['str-expiry-month'],
				'exp_year' => $req['str-expiry-year'],
				'cvc' => $req['cvc']
			),
		);

		try {
			$token = \Stripe\Token::create( $args );
		}
		catch ( Exception $e )
		{
			$body = $e->getJsonBody();

			$token = array(
				'errors' => $body['error'],
			);
		}

		if( array_key_exists( 'errors', $token) ) {
			$show_card_error = true;
		} else {
			$customer_obj = get_stripe_customer( $_GET['cust_id'] );
			$customer_obj->source = $token->id;
			$customer_obj->save();
		}

	} else {
		if( $req['subscription-method'] == 'gift') {
			update_field( 'sub_recipient_first_name', $req['first-name'], $req['subscription-post-id'] );
			update_field( 'sub_recipient_last_name', $req['last-name'], $req['subscription-post-id'] );
			update_field( 'sub_recipient__address_1', $req['recipient-street-address'], $req['subscription-post-id'] );
			update_field( 'sub_recipient__address_2', $req['recipient-address-line-2'], $req['subscription-post-id'] );
			update_field( 'sub_recipient__city', $req['recipient-city'], $req['subscription-post-id'] );
			update_field( 'sub_recipient__county', $req['recipient-county'], $req['subscription-post-id'] );
			update_field( 'sub_recipient__postcode', $req['recipient-postcode'], $req['subscription-post-id'] );
		} else {
			update_field( 'sub_first_name', $req['first-name'], $req['subscription-post-id'] );
			update_field( 'sub_last_name', $req['last-name'], $req['subscription-post-id'] );
			update_field( 'sub_delivery_address_1', $req['recipient-street-address'], $req['subscription-post-id'] );
			update_field( 'sub_delivery_address_2', $req['recipient-address-line-2'], $req['subscription-post-id'] );
			update_field( 'sub_delivery_city', $req['recipient-city'], $req['subscription-post-id'] );
			update_field( 'sub_delivery_county', $req['recipient-county'], $req['subscription-post-id'] );
			update_field( 'sub_delivery_postcode', $req['recipient-postcode'], $req['subscription-post-id'] );
		}
	}
} ?>

<section class="page-section white form-page">
	<div class="row">
		<div class="small-12 medium-9 medium-centered large-6 columns">
			<form class="subscription-form" method="post">
				<ul class="sign-up-form">
					<?php
					$subscription_obj = get_subscription_by_cust_id( $_GET['cust_id'] );

					$subscription_details = get_post_custom( $subscription_obj[0]->ID );

					if( $subscription_details['sub_subscription_method'][0] == 'gift' ) {
						$args = array(
							array(
								'label' => 'First Name',
								'type'	=> 'text',
								'class' => 'first-name',
								'data-stripe' => 'recipient-first-name',
								'value' => $subscription_details['sub_recipient_first_name'][0]
							),
							array(
								'label' => 'Last Name',
								'type'	=> 'text',
								'class' => 'last-name',
								'data-stripe' => 'recipient-last-name',
								'value' => $subscription_details['sub_recipient_last_name'][0]
							),
							array(
								'label' => 'Street Address',
								'type'	=> 'text',
								'class' => 'recipient-street-address',
								'data-stripe' => 'recipient-street-address',
								'value' => $subscription_details['sub_recipient_address_1'][0]
							),
							array(
								'label' => 'Address Line 2',
								'type'	=> 'text',
								'class' => 'recipient-address-line-2',
								'data-stripe' => 'recipient-address-line-2',
								'value' => $subscription_details['sub_recipient_address_2'][0]
							),
							array(
								'label' => 'City',
								'type'	=> 'text',
								'class' => 'recipient-city',
								'data-stripe' => 'recipient-city',
								'value' => $subscription_details['sub_recipient_city'][0]
							),
							array(
								'label' => 'County',
								'type'	=> 'text',
								'class' => 'recipient-county',
								'data-stripe' => 'recipient-county',
								'value' => $subscription_details['sub_recipient_county'][0]
							),
							array(
								'label' => 'Post Code',
								'type'	=> 'text',
								'class' => 'recipient-postcode',
								'data-stripe' => 'recipient-postcode',
								'value' => $subscription_details['sub_recipient_postcode'][0]
							),
							array(
								'label' => $subscription_details['sub_customer_id'][0],
								'type'	=> 'hidden',
								'class' => 'customer_id',
								'data-stripe' => 'customer-id',
								'value' => '',
							),
							array(
								'label' => $subscription_details['sub_subscription_method'][0],
								'type'	=> 'hidden',
								'class' => 'subscription-method',
								'data-stripe' => 'subscription-method',
								'value' => '',
							),
							array(
								'label' => $subscription_obj[0]->ID,
								'type'	=> 'hidden',
								'class' => 'subscription-post-id',
								'data-stripe' => 'subscription-post-id',
								'value' => '',
							),
							array(
								'label' => 'Update Details',
								'type'	=> 'submit',
								'class' => 'update-details',
								'data-stripe' => 'update-details',
								'value' => '',
							),
						);
					} else {
						$args = array(
							array(
								'label' => 'First Name',
								'type'	=> 'text',
								'class' => 'first-name',
								'data-stripe' => 'first-name',
								'value' => $subscription_details['sub_first_name'][0]
							),
							array(
								'label' => 'Last Name',
								'type'	=> 'text',
								'class' => 'last-name',
								'data-stripe' => 'last-name',
								'value' => $subscription_details['sub_last_name'][0]
							),
							array(
								'label' => 'Street Address',
								'type'	=> 'text',
								'class' => 'delivery-street-address',
								'data-stripe' => 'delivery-street-address',
								'value' => $subscription_details['sub_delivery_address_1'][0]
							),
							array(
								'label' => 'Address Line 2',
								'type'	=> 'text',
								'class' => 'delivery-address-line-2',
								'data-stripe' => 'delivery-address-line-2',
								'value' => $subscription_details['sub_delivery_address_2'][0]
							),
							array(
								'label' => 'City',
								'type'	=> 'text',
								'class' => 'delivery-city',
								'data-stripe' => 'delivery-city',
								'value' => $subscription_details['sub_delivery_city'][0]
							),
							array(
								'label' => 'County',
								'type'	=> 'text',
								'class' => 'delivery-county',
								'data-stripe' => 'delivery-county',
								'value' => $subscription_details['sub_delivery_county'][0]
							),
							array(
								'label' => 'Post Code',
								'type'	=> 'text',
								'class' => 'delivery-postcode',
								'data-stripe' => 'delivery-postcode',
								'value' => $subscription_details['sub_delivery_postcode'][0]
							),
							array(
								'label' => $subscription_details['sub_customer_id'][0],
								'type'	=> 'hidden',
								'class' => 'customer_id',
								'data-stripe' => 'customer-id',
								'value' => '',
							),
							array(
								'label' => $subscription_details['sub_subscription_method'][0],
								'type'	=> 'hidden',
								'class' => 'subscription-method',
								'data-stripe' => 'subscription-method',
								'value' => '',
							),
							array(
								'label' => $subscription_obj[0]->ID,
								'type'	=> 'hidden',
								'class' => 'subscription-post-id',
								'data-stripe' => 'subscription-post-id',
								'value' => '',
							),
							array(
								'label' => 'Update Details',
								'type'	=> 'submit',
								'class' => 'update-details',
								'data-stripe' => 'update-details',
								'value' => '',
							),
						);
					}

					echo es_form_builder ( $args );?>
				</ul>
			</form>
		</div>
	</div>
</section>
<section class="page-section patterned">
	<div class="row">
		<div class="small-12 columns text-center">
			<h2>Update Payment Details</h2>
		</div>
	</div>
</section>
<section class="page-section white form-page">
	<div class="row">
		<div class="small-12 medium-9 medium-centered large-6 columns">
			<?php if( $subscription_details['sub_subscription_method'][0] == 'subscription' ) {
				$customer_obj = get_stripe_customer( $_GET['cust_id'] ); ?>
				<form class="subscription-form" method="post">
					<ul class="sign-up-form">
						<li class="input-type-text show-for-both class-str-card-number">
							<label for="str-card-number">Card Number</label>
							<input type="text" name="str-card-number" data-stripe="str-card-number" value="************<?php echo $customer_obj->sources->data[0]->last4 ?>">
						</li>
						<li class="input-type-section show-for-both class-expiry-date">
							<h4><strong>Expiration (MM/YY)</strong></h4>
						</li>
						<li class="input-type-text show-for-both class-str-expiry-date">
							<label for="str-expiry-date">mm</label>
							<input type="text" name="str-expiry-month" data-stripe="exp_month" value="<?php echo $customer_obj->sources->data[0]->exp_month; ?>">
						</li>
						<li class="input-type-text show-for-both class-str-expiry-date">
							<label for="str-expiry-date">yy</label>
							<input type="text" name="str-expiry-year" data-stripe="exp_year" value="<?php echo substr( $customer_obj->sources->data[0]->exp_year, -2 ); ?>">
						</li>
						<li class="input-type-text show-for-both class-str-security-code">
							<label for="str-security-code">Security Code (CVC)</label>
							<input type="text" name="cvc" data-stripe="cvc" value="">
						</li>
						<li class="input-type-submit show-for-both class-update-card-details">
							<button class="button" type="submit" name="update-card-details" id="update-card-details" value="update-card-details">Update Card Details</button>
						</li>
					</ul>
				</form>
			<?php } ?>
		</div>
	</div>
</section>